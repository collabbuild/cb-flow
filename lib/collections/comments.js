// Initiate collection for comments
// Steps on setting up new collection
// 1: initiate new collection (see below)
// 2: publish in /server/publications.js
// 3: subscribe in /lib/router.js
Comments = new Mongo.Collection('comments');

Meteor.methods({
  // called in comment-submit.js in the submit event
  commentInsert: function(commentAttributes) {
    // check if the attributes are strings
    check(this.userId, String);
    check(commentAttributes, {
      jobId: String,
      body: String
    });

    // set 'user' to the current logged in user
    var user = Meteor.user();

    // set 'job' to the job cursor that matches the jobId
    var job = Jobs.findOne(commentAttributes.jobId);

    // if the job does not exist, then prevent the user from submmitting a comment
    if (!job)
      throw new Meteor.Error('invalid-comment', 'You must comment on a job');
    
    // add user properties to the comment document
    comment = _.extend(commentAttributes, {
      userId: user._id,
      author: user.username,
      submitted: new Date()
    });

    // update the post with the number of comments
    Jobs.update(comment.jobId, {$inc: {commentsCount: 1}});

    // create the comment, save the id
    comment._id = Comments.insert(comment);

    // now create a notification, informing the user that there's been a comment
    createCommentNotification(comment);

    return comment._id;
  },


  // called in comment-submit.js in the submit event
  commentInsertForLink: function(commentAttributes) {
    // check if the attributes are strings
    check(Meteor.userId(), String);
    check(commentAttributes, {
      linkId: String,
      body: String
    });

    // set 'user' to the current logged in user
    var user = Meteor.user();

    // set 'job' to the job cursor that matches the jobId
    var link = Links.findOne(commentAttributes.linkId);
    var rule = Rules.findOne(commentAttributes.linkId);
    var cost = Costs.findOne(commentAttributes.linkId);

    // if the job does not exist, then prevent the user from submmitting a comment
    if (!link && !rule && !cost)
      throw new Meteor.Error('invalid-comment', 'You must comment on a link or rule');
    
    // add user properties to the comment document
    comment = _.extend(commentAttributes, {
      userId: user._id,
      author: user.username,
      submitted: new Date()
    });

    if(link) {
    // update the post with the number of comments
      Links.update(comment.linkId, {$inc: {commentsCount: 1}});
      Links.update(comment.linkId, {$set: {updated: new Date()}});
    }
    else if(rule) {
      Rules.update(comment.linkId, {$inc: {commentsCount: 1}});
      Rules.update(comment.linkId, {$set: {updated: new Date()}});    
    }
    else {
      Costs.update(comment.linkId, {$inc: {commentsCount: 1}});
      Costs.update(comment.linkId, {$set: {updated: new Date()}});
    }

    // create the comment, save the id
    comment._id = Comments.insert(comment);

    // now create a notification, informing the user that there's been a comment
    // createCommentNotification(comment);

    return comment._id;
  }
});