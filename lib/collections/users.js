// error reporting for missing fields
validateUser = function(user) {
  var errors = {};
  if (!user.firstName)
    errors.firstName = "Please enter your first name";
  if (!user.lastName)
    errors.lastName = "Please enter your last name";
  if (!user.email)
    errors.email = "Please enter your email";
  return errors;
}
if (Meteor.isServer) {
Meteor.methods({
		  
  'addNewEmail': function(address)
  {
	Accounts.addEmail(Meteor.userId(), address);

  }, 
  
  'removeOldEmail': function(address)
  {
	Accounts.removeEmail(Meteor.userId(), address);
  },
  
  'updateUsername': function(newUsername)
  {
	Accounts.setUsername(Meteor.userId(), newUsername)
  },
});
}
