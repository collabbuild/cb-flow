// initiate new collection
Notifications = new Mongo.Collection('notifications');

// 
Notifications.allow({
  // update: function(userId, doc, fieldNames) {
  //   return ownsDocument(userId, doc) && 
  //     fieldNames.length === 1 && fieldNames[0] === 'read';
  // }
  update: function() {
    return true;
  }
});

//  
createCommentNotification = function(comment) {
  var job = Jobs.findOne(comment.jobId);
  var jobAssignee = job.assignee;
  var projectId = job.project;
  var project = Projects.findOne(projectId);
  var assignmentArray = project.assignments;

  // assignment object has two fields: title and leader
  function getLeaders(assignmentObject) {
    if(jobAssignee === assignmentObject.title) {
      // return the userId of the users subscribed
      return assignmentObject.leader;
    } else {
      return "filterOut";
    }
  }

  function notifyLeaders(varUserId) {
    if (comment.userId != varUserId) {
      var notificationId = Notifications.insert({
        userId: varUserId,
        jobId: job._id,
        commentId: comment._id,
        commenterName: comment.author,
        read: false
      });

      return notificationId;
    }    
  }

  
  var leaders = assignmentArray.map(getLeaders);
  var assignmentLeaders = leaders.filter(function(n) { return n != "filterOut" });
  if(assignmentLeaders[0].length > 0) {    
    var leadersArray = assignmentLeaders[0].toString().split(",");
    var notificationArray = leadersArray.map(notifyLeaders);
  }

  //   // notifies the owner of the job that a comment was submitted
  //   if (comment.userId !== job.userId) {
  //     Notifications.insert({
  //       userId: job.userId,
  //       jobId: job._id,
  //       commentId: comment._id,
  //       commenterName: comment.author,
  //       read: false
  //     });
  //   }

};

createActivityNotification = function(activity) {
  var job = Jobs.findOne(activity.jobId);

  if (activity.userId !== job.userId) {
    Notifications.insert({
      userId: job.userId,
      jobId: job._id,
      activityId: activity._id,
      activityOwner: activity.author,
      read: false
    });
  }

}

Meteor.methods({
  clearNotifications: function() {
    var currentUserId = Meteor.userId();

    Notifications.update({userId: currentUserId}, {$set: {read: true}}, {multi: true});
  }


})
