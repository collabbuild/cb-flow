// create projects collection
Projects = new Mongo.Collection('projects');

validateProject = function (project) {
  var errors = {};
  if (!project.title)
    errors.title = "Please fill in a project name";
  if (!project.deadline)
    errors.deadline = "Please set a deadline";
  return errors;
}

Meteor.methods({
	projectInsert: function(projectAttributes) {
		check(Meteor.userId(), String);
		check(projectAttributes, {
			title: String,
      deadline: String,
      phases: Array,
      currency: String,
      invitations: String
		});
	
		// var errors = validateProject(projectAttributes);
	  //   if (errors.title)
	  //     throw new Meteor.Error('invalid-project', "You must set a project name");

	  // set the user to the logged in user
	  var user = Meteor.user();

	  // add user properties
	  var project = _.extend(projectAttributes, {
	  	userId: user._id,
	  	owner: user.username,
	  	submitted: new Date(),
	  	members: [],
	  	assignments: [
        {title: "Owner", leader: []},
        {title: "Design", leader: []},
        {title: "Contractor", leader: []},
        {title: "Miscellaneous", leader: []}
      ],
      invited: [],
      transparent: false,
      admin: [],
      closed: false
	  });

	  // creates new project and returns an id
	  var projectId = Projects.insert(project);
	  // Projects.update(projectId, {$addToSet: {members: project.userId}});
    Projects.update(projectId, {$addToSet: {admin: project.userId}});

	  return{
	  	_id: projectId
	  };
  },

  inviteUser: function(projectId, userId) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(userId, String);

    Projects.update(projectId, {$addToSet: {invited: userId}})
  },

  uninviteUser: function(projectId, userId) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(userId, String);

    Projects.update(projectId, {$pull: {invited: userId}});
  },

  makeAdmin: function(projectId, userId) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(userId, String);

    Projects.update(projectId, {$pull: {members: userId}});
    Projects.update(projectId, {$addToSet: {admin: userId}});
  },

  addUser: function(projectId, userId) {
  	check(Meteor.userId(), String);
  	check(projectId, String);
    check(userId, String);
  	var adminArray = Projects.findOne(projectId).admin;
    var adminCount = adminArray.length;
	if ( adminCount < 1 ){
  		Projects.update(projectId, {$addToSet: {admin: userId}});
	} else {
		Projects.update(projectId, {$addToSet: {members: userId}});
	}
    Projects.update(projectId, {$pull: {invited: userId}});
  },

  removeUser: function(projectId, userId) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(userId, String);
    
    var adminArray = Projects.findOne(projectId).admin;
    var adminCount = adminArray.length;
    var membersArray = Projects.findOne(projectId).members;
    var membersCount = membersArray.length;
    var assignmentsArray = Projects.findOne(projectId).assignments;
    var assignmentsCount = assignmentsArray.length;

    // only run the pull command if the member is in admin
    for(i = 0; i < adminCount; i++) {
      var n = userId.localeCompare(adminArray[i]);
      if(n === 0) {
        Projects.update(projectId, {$pull: {admin: userId}});
      }
    }

    // only run the pull command if the member is in members
    for(i = 0; i < membersCount; i++) {
      var n = userId.localeCompare(membersArray[i]);
      if(n === 0) {
        Projects.update(projectId, {$pull: {members: userId}});
      }
    }

    for(i = 0; i < assignmentsCount; i++) {
      Projects.update(
        {_id: projectId, "assignments.leader": userId}, 
        { $pull: {"assignments.$.leader": userId} }
      );
    }


    Projects.update(projectId, {$addToSet: {invited: userId}});
  },

  addAssignment: function(projectId, assignment) {
  	check(Meteor.userId(), String);
  	check(projectId, String);
  	check(assignment, {
  		title: String,
  		leader: Array
  	});

  	Projects.update(projectId, {$addToSet: { assignments: assignment}} );
  },

  addLeader: function(projectId, assignment) {
  	check(Meteor.userId(), String);
  	check(projectId, String);
  	check(assignment, {
  		title: String,
  		leader: String
  	});

  	var assignmentTitle = assignment.title;
  	var assignmentLeader = assignment.leader;

  	Projects.update(
  		{_id: projectId, "assignments.title": assignmentTitle}, 
  		{ $addToSet: {"assignments.$.leader": assignmentLeader} }
  	);
  },

  unassignLeader: function(projectId, assignment) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(assignment, {
      title: String,
      leader: String
    });

    var assignmentTitle = assignment.title;
    var assignmentLeader = assignment.leader;

    Projects.update(
      {_id: projectId, "assignments.title": assignmentTitle}, 
      { $pull: {"assignments.$.leader": assignmentLeader} }
    );
  },

  deleteAssignment: function(projectId, assignment) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(assignment, {
      title: String
    });

    var assignmentTitle = assignment.title;

    Projects.update(projectId, {$pull: { assignments: {title: assignmentTitle} } } );
  },
  
  declineUser: function(projectId, userId) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(userId, String);

    Projects.update(projectId, {$pull: {invited: userId}});
  },

  closeProject: function(projectId) {
    check(Meteor.userId(), String);
    check(projectId, String);

    Projects.update(projectId, {$set: {closed: true}});
  },

  reopenProject: function(projectId) {
    check(Meteor.userId(), String);
    check(projectId, String);

    Projects.update(projectId, {$set: {closed: false}});
  },

  setPrivacy: function(projectId, newSetting) {
    check(Meteor.userId(), String);
    check(projectId, String);
    check(newSetting, String);

    Projects.update(projectId, {$set: {invitations: newSetting}});
  }  

});