// Initiate collection for comments
// Steps on setting up new collection
// 1: initiate new collection (see below)
// 2: publish in /server/publications.js
// 3: subscribe in /lib/router.js
Activities = new Mongo.Collection('activities');

Meteor.methods({
  // called in comment-submit.js in the submit event
  // this method is called in the following files:
  // 
  activityInsert: function(activityAttributes) {
    // check if the attributes are strings
    check(this.userId, String);
    check(activityAttributes, {
      jobId: String,
      activityBody: String
    });

    // set 'user' to the current logged in user
    var user = Meteor.user();

    // set 'job' to the job cursor that matches the jobId
    var job = Jobs.findOne(activityAttributes.jobId);
    var projectId = job.project;

    // if the job does not exist, then prevent the user from submmitting a comment
    if (!job)
      throw new Meteor.Error('invalid-comment', 'You must comment on a job');
    
    // add user properties to the comment document
    activity = _.extend(activityAttributes, {
      userId: user._id,
      author: user.username,
      submitted: new Date(),
      project: projectId
    });

    // update the post with the number of comments
    // Jobs.update(activity.jobId, {$inc: {activitiesCount: 1}});

    // create the comment, save the id
    activity._id = Activities.insert(activity);

    // now create a notification, informing the user that there's been a comment
    // see notifications.js collection file
    // createActivityNotification(activity);

    return activity._id;
  }
});