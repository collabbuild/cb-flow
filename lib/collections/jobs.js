// create jobs collection
Jobs = new Mongo.Collection('jobs');

// allow and deny the owner of a job to edit or delete
Jobs.allow({
  update: function(userId, job) { return ownsDocument(userId, job); },
  remove: function(userId, job) { return ownsDocument(userId, job); }
});

Jobs.deny({
  update: function(userId, job, fieldNames) {
    // may only edit the following fields
    return (_.without(fieldNames, 'title', 'assignee', 'deadline', 'description', 'tags', 'predecessor', 'descendant').length > 0);
  }
});

Jobs.deny({
  update: function(userId, job) {
    return _.without(fieldNames, 'isChecked');
  }
});

Jobs.deny({
  update: function(userId, job, fieldNames, modifier) {
    var errors = validateJob(modifier.$set);
    return errors.title;
  }
});

// error reporting for missing fields
validateJob = function(job) {
  var errors = {};
  if (!job.title)
    errors.title = "Please fill in a title";
  if (!job.assignee)
    errors.assignee = "Please specify the discipline";
  if (!job.deadline)
    errors.deadline = "Please specify a deadline";
  return errors;
}

Meteor.methods({
  // method for creating new job-items
  jobInsert: function(projectId, jobAttributes, newTags) {
    // check if user is logged in by making sure the current userId is a string
    check(Meteor.userId(), String);
    check(projectId, String);
    check(newTags, Array);
    // check that the incoming attributes are in the right format
    check(jobAttributes, {
      title: String,
      assignee: String,
      deadline: String,
      description: String,
      predecessor: Array,
      dropboxlink: String
    });

    // call function above methods block
    // if the following fields are empty, prevent user from submitting the job until field is completed
    var errors = validateJob(jobAttributes);
    if (errors.title)
      throw new Meteor.Error('invalid-job', "You must set a title");
    if (errors.assignee)
      throw new Meteor.Error('invalid-assignee', "You must set an assignee");
    if (errors.deadline)
      throw new Meteor.Error('invalid-deadline', "You must set a deadline");

    // add descendant field and tags field to attributes with an empty array
    var jobArrays = _.extend(jobAttributes, {
      descendant: [],
      tags: []
    });

    // loop for lock status
    // get the new predecessor jobs from user input
    var predecessorJobs = jobAttributes.predecessor;
    // get the number of items in array
    var predecessorJobsLength = predecessorJobs.length;
    if(predecessorJobsLength > 0) {
      for(i = 0; i < predecessorJobsLength; i++) {
        predecessorArrayObject = predecessorJobs[i].toString();
        // add isLocked field dependent on if predecessor is set to 'None' or not
        if(predecessorArrayObject === "None" || predecessorArrayObject === "") {
          var jobLock = _.extend(jobAttributes, {
            isLocked: false,
          });
        } else {
          var jobLock = _.extend(jobAttributes, {
            isLocked: true,
          });      
        }
      }
    }

    // set the user to the logged in user
    var user = Meteor.user();
    var jobCount = Jobs.find({project: projectId}).count();

    // add user properties and initiate commentCount
    var job = _.extend(jobAttributes, {
      userId: user._id, 
      author: user.username,
      submitted: new Date(),
      commentsCount: 0,
      isChecked: false,
      deleted: false,
      project: projectId,
      flow: projectId,
      jobNo: (jobCount + 1)
    });

    // create var jobId and insert the new job into the collection
    var jobId = Jobs.insert(job);

    // add user submitted tags individually to avoid
    // grouping behavior.  This is a temporary solution.
    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Jobs.update(jobId, {$addToSet: {tags: newTags[i]}});
    }

    // update predecessor jobs
    if(predecessorJobsLength > 0) {
      for(i = 0; i < predecessorJobsLength; i++) {
        var predecessorArrayObject = predecessorJobs[i].toString();
        // if there is a job doc that matches the id passed through,
        // then update the predecessor job with the id of the newly
        // created job
        if(Jobs.find({_id: predecessorArrayObject}).count() > 0) { 
          Jobs.update(predecessorArrayObject, {$addToSet: { descendant: [jobId] } });
        };
      }
    }

    return {
      _id: jobId
    };
  },


  // method for updating job properties
  jobUpdate: function(jobProperties, currentJobId, addTags) {
    check(Meteor.userId(), String);
    check(currentJobId, String);
    check(addTags, Array);
    check(jobProperties, {
      title: String,
      assignee: String,
      deadline: String,
      description: String,
      predecessor: Array,
      dropboxlink: String
    });

    var errors = validateJob(jobProperties);
    if (errors.title)
      throw new Meteor.Error('invalid-job', "You must set a title");
    if (errors.assignee)
      throw new Meteor.Error('invalid-assignee', "You must set an assignee");
    if (errors.deadline)
      throw new Meteor.Error('invalid-deadline', "You must set a deadline");

    // loop to unset old predecessors
    var oldPredecessorJobs = Jobs.findOne({_id: currentJobId}).predecessor;
    var oldPredecessorJobsLength = oldPredecessorJobs.length;
    if(oldPredecessorJobsLength > 0) {
      for(i = 0; i < oldPredecessorJobsLength; i++) {
        predecessorArrayObject = oldPredecessorJobs[i].toString();
        // remove the currentJobId from the descendant list in the predecessorJob
        Jobs.update(predecessorArrayObject, {$pull: { descendant: [currentJobId] } });
      }
    }

    // loop to set current job as a decendant to the predecessor jobs
    var newPredecessorJobs = jobProperties.predecessor;
    var newPredecessorJobsLength = newPredecessorJobs.length;
    if(newPredecessorJobsLength > 0) {
      for(i = 0; i < newPredecessorJobsLength; i++) {
        var predecessorArrayObject = newPredecessorJobs[i].toString();
        // adds the currentJobId to the descendant list in the predecessorJob
        Jobs.update(predecessorArrayObject, {$addToSet: { descendant: [currentJobId] } });
      }
    }
    
    // loop to check if other predecessors are checked/done
    // if there are no predecessors then unlock job
    if(newPredecessorJobsLength === 0) {
      Jobs.update(currentJobId, {$set: {isLocked: false}});
    } else {
      var i = 0; // reset i to zero
      while(i < newPredecessorJobsLength) {
        var predecessorArrayObject = newPredecessorJobs[i].toString();
        var currentPredecessorJob = Jobs.findOne({_id: predecessorArrayObject});
        // if predjob is checked, +1 to i and check next
        // job in array
        if(currentPredecessorJob.isChecked) {
          i++;
        } else { // predjob is not checked
          Jobs.update(currentJobId, {$set: {isChecked: false}});
          Jobs.update(currentJobId, {$set: {isLocked: true}});
          break;
        }
        // if i equals the length of array, this means that
        // all predjobs are checked and the current job should
        // be unlocked.
        if(i === newPredecessorJobsLength) {
          Jobs.update(currentJobId, {$set: {isLocked: false}});
        }
      }
    }

    Jobs.update(currentJobId, {$set: jobProperties});
    var tagsCount = addTags.length;
    for(i=0; i<tagsCount; i++) {
      Jobs.update(currentJobId, {$addToSet: {tags: addTags[i] } });  
    }
    
    // Jobs.update(currentJobId, {$pull: {tags: [] } });
    
  },

  setChecked: function(currentJobId, property, descendantArray) {
    check(Meteor.userId(), String);
    check(currentJobId, String);
    check(property, Boolean);
    check(descendantArray, Array);

    // mark the job as 'done'
    Jobs.update(currentJobId, {$set: {isChecked: property} });

    // get limit for loop below
    var descendants = descendantArray.length;

    // true: update all descendants and unlock (isLocked: false)
    if(property) {
      // if no descendants, do not run loop
      if(descendants > 0) {
        // iterate through each descendant in descendant in array
        for (i=0; i < descendants; i++) {
          var descendantId = descendantArray[i].toString();
          var descendantJob = Jobs.findOne( {_id: descendantId} );
          // get the predecessorArray of the descendant job
          var descendantPredArray = descendantJob.predecessor;
          var descendantPredArrayLength = descendantPredArray.length;
          
          // check if all other predecessors of the descendant is finished(isChecked)
          var j = 0;
          if (descendantPredArrayLength > 0) {
            while(j < descendantPredArrayLength) {
              var predecessorId = descendantPredArray[j].toString();
              var currentPredecessorJob = Jobs.findOne({_id: predecessorId});
              if(currentPredecessorJob.isChecked) {
                j++;
              } else {
                j = (descendantPredArrayLength +1);
              }

              // if all predecessors are done/checked then i will equal array length
              if(j === descendantPredArrayLength) {
                Jobs.update(descendantId, {$set: {isLocked: false}});
              }
            }
          }
          // Jobs.update(descendantId, {$set: {isLocked: false} });
        }
      }
    }

    // false: update all descendants and lock (isLocked: true)
    if(!property) {
      if(descendants > 0) {
        for (i=0; i < descendants; i++) {
          var descendantId = descendantArray[i].toString();
          Jobs.update(descendantId, {$set: {isChecked: false} });
          Jobs.update(descendantId, {$set: {isLocked: true} });
        }
      }
    }
  },

  jobDelete: function(currentJobId, predecessorArray, descendantArray) {
    check(Meteor.userId(), String);
    check(currentJobId, String);
    check(predecessorArray, Array);
    check(descendantArray, Array);

    var descendantLength = descendantArray.length;
    var predecessorLength = predecessorArray.length;

    // remove from predecessor job
    for(i=0; i<predecessorLength; i++) {
      var predecessorId = predecessorArray[i].toString();
      Jobs.update(predecessorId, {$pull: {descendant: currentJobId} });
    }

    // remove from descendants
    for(i=0; i<descendantLength; i++) {
      var descendantId = descendantArray[i].toString();
      Jobs.update(descendantId, {$pull: {predecessor: currentJobId} });
      var descendantsPred = Jobs.findOne(descendantId).predecessor;
      if(descendantsPred.length === 0) {
        Jobs.update(descendantId, {$set: {isLocked: false} });
      }
    }

    // delete current job from collection
    // Jobs.remove(currentJobId);

    // changed deleted field to true
    Jobs.update(currentJobId, {$set: {deleted: true}});
    // clear predecessor and descendant array
    Jobs.update(currentJobId, {$set: {predecessor: []}});
    Jobs.update(currentJobId, {$set: {descendant: []}});

  },

  jobUndelete: function(currentJobId) {
    check(Meteor.userId(), String);
    check(currentJobId, String);

    Jobs.update(currentJobId, {$set: {deleted: false}});
  },

  tagDelete: function(currentJobId, tagName) {
    check(Meteor.userId(), String);
    check(currentJobId, String);
    check(tagName, String);
    Jobs.update({_id: currentJobId}, {$pull: {tags: tagName } }); 
  }
});
