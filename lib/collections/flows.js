// create flows collection
Flows = new Mongo.Collection('flows');


//methods for Flows collection
Meteor.methods({
	flowInsert: function(predecessorArray, flowName, projectId) {
		check(Meteor.userId(), String);
		check(predecessorArray, Array);
		check(flowName, String);
		check(projectId, String);

		var predecessorNameArray = predecessorArray.map(function(p) {
			return Jobs.findOne(p).title;
		});

		var user = Meteor.user();

		// function to get job details to take a snapshot of the flow when 
		// the flow id is created
		function getJobDetails(varId) {

			var predJob = Jobs.findOne(varId);
			var predArray = predJob.predecessor;
			var predNameArray = predArray.map(function(m) {
				return Jobs.findOne(m).title;
			});
			var predTrim = predNameArray.filter(function(n) {
				if(predecessorNameArray.contains(n)) {
					return n;
				}
			});

			var jobDetails = {
				_id: varId,
				title: predJob.title,
				predecessor: predTrim,
				description: predJob.description,
				tags: predJob.tags
			};

			return jobDetails;
		}

		// get the array of jobs objects to save in the flow attributes
		var jobArray = predecessorArray.map(getJobDetails);

		var flow = {
			publishers: [],
			userId: user._id, //original author
      author: user.username, //original author
			submitted: new Date(),
			updated: new Date(),
			name: flowName,
			project: projectId,
			jobs: jobArray,
			public: false,
			published: false
		};

		// insert new flow doc and generate id
		var flowId = Flows.insert(flow);


		var predecessorLength = predecessorArray.length;
		// update predecessor jobs to set flow to new flow id
		for(i = 0; i < predecessorLength; i++){
			var predJobId = predecessorArray[i];
			Jobs.update(predJobId, {$set: {flow: flowId}});
		}

		return {
			_id: flowId
		};

	},

	flowUpdate: function(flowId, jobId) {
		check(Meteor.userId(), String);
		check(flowId, String);
		check(jobId, String);

		function getName(obj) {
			var theName = obj.title;
			return theName;
		}

		jobArray = Flows.findOne(flowId).jobs;
		nameArray = jobArray.map(getName);

		function getJobDetails(varId) {

			var predJob = Jobs.findOne(varId);
			var predArray = predJob.predecessor;
			var predNameArray = predArray.map(function(m) {
				return Jobs.findOne(m).title;
			});			
			var predTrim = predNameArray.filter(function(n) {
				if(nameArray.contains(n)) {
					return n;
				}
			});

			var jobDetails = {
				_id: varId,
				title: predJob.title,
				predecessor: predTrim,
				description: predJob.description,
				tags: predJob.tags
			};

			return jobDetails;
		}

		var currentUserId = Meteor.userId();
		var jobDet = getJobDetails(jobId);

		Jobs.update(jobId, {$set: {flow: flowId}});

		Flows.update(flowId, {$addToSet: {jobs: jobDet}});
		Flows.update(flowId, {$set: {updated: new Date()}});
	},

	flowLoad: function(flowId, projectId) {
		check(Meteor.userId(), String);
		check(flowId, String);
		check(projectId, String);

		var user = Meteor.user();

		var copyOfFlow = Flows.findOne(flowId);
		copyOfFlow._id = new Meteor.Collection.ObjectID().valueOf();

		var newFlowId = Flows.insert(copyOfFlow);
		Flows.update(newFlowId, {$set: {project: projectId}});
		Flows.update(newFlowId, {$set: {submitted: new Date()}});
		Flows.update(newFlowId, {$set: {updated: new Date()}});
		Flows.update(newFlowId, {$set: {name: "Copy of " + copyOfFlow.name}});
		Flows.update(newFlowId, {$set: {public: false}});
		Flows.update(newFlowId, {$set: {published: false}});
		Flows.update(newFlowId, {$set: {userId: user._id}});



		function insertFlowJobs(jobObj) {

			var jobAttributes = {
				title: jobObj.title,
	      assignee: "",
	      deadline: "",
	      description: jobObj.description,
	      predecessor: jobObj.predecessor,
	      unit: "",
	      quantity: "",
	      unitcost: "",
	      descendant: [],
      	tags: jobObj.tags,
	      userId: user._id, 
	      author: user.username, 
	      submitted: new Date(),
	      commentsCount: 0,
	      isChecked: false,
	      deleted: false,
	      project: projectId,
	      flow: newFlowId
    	}

  		var predecessorJobs = jobAttributes.predecessor;
			var predecessorJobsLength = predecessorJobs.length;
			if(predecessorJobsLength > 0) {
	      for(i = 0; i < predecessorJobsLength; i++) {
	        predecessorArrayObject = predecessorJobs[i].toString();
	        // add isLocked field dependent on if predecessor is set to 'None' or not
	        if(predecessorArrayObject === "None" || predecessorArrayObject === "") {
	          var job = _.extend(jobAttributes, {
	            isLocked: false,
	          });
	        } else {
	          var job = _.extend(jobAttributes, {
	            isLocked: true,
	          });
	        }
	      }
	    } else {
	  		  var job = _.extend(jobAttributes, {
	          isLocked: false,
	        });
	    }

			var jobId = Jobs.insert(job);
			Flows.update({_id: newFlowId, "jobs.title": jobAttributes.title}, {$set: {"jobs.$._id": jobId} })

			return {
				_id: jobId
			};
		}

		function setDescendants(varId) {
			var predecessorNameArray = Jobs.findOne(varId).predecessor;
			var currentId = Jobs.findOne(varId)._id;
			var predecessorNameArrayLength = predecessorNameArray.length;

	    if(predecessorNameArrayLength > 0) {
	      for(i = 0; i < predecessorNameArrayLength; i++) {
	        var predecessorName = predecessorNameArray[i].toString();
	        Jobs.update(varId, {$pull: {predecessor: predecessorName}});
	        var predecessorId = Jobs.findOne({title: predecessorName, project: projectId})._id;
	        Jobs.update(varId, {$addToSet: {predecessor: predecessorId}});


	        Jobs.update(predecessorId, {$addToSet: {descendant: currentId}});	       
	      }
	    }
	  }

		var jobArray = Flows.findOne(flowId).jobs;
		var insertedJobs = jobArray.map(insertFlowJobs);
		insertedJobs.map(setDescendants);
	},

	flowPublish: function(oldFlowId) {
		check(Meteor.userId(), String);
		check(oldFlowId, String);

		var currentUserId = Meteor.userId();
		var copyOfFlow = Flows.findOne(oldFlowId);
		copyOfFlow._id = new Meteor.Collection.ObjectID().valueOf();

		var flowId = Flows.insert(copyOfFlow);

		// update the newly created flow
		// public: true will make the flow available to all users
		Flows.update(flowId, {$set: {public: true}});
		// publishers keep track of all contributors
		Flows.update(flowId, {$addToSet: {publishers: currentUserId}});

		Flows.update(flowId, {$set: {userId: ""}});
		Flows.update(flowId, {$set: {project: ""}});
		Flows.update(flowId, {$set: {submitted: new Date()}});


		// update the old flow
		// once published is true, flow cannot be published again
		Flows.update(oldFlowId, {$set: {published: true}});

		return {
			_id: flowId
		};
	},

	flowRename: function(flowId, newName) {
		check(Meteor.userId(), String);
		check(flowId, String);
		check(newName, String);

		Flows.update(flowId, {$set: {name: newName}});
	},

	flowDelete: function(flowId) {
		check(Meteor.userId(), String);
		check(flowId, String);

		var projectId = Flows.findOne(flowId).project;

		function revertFlow(varJobId) {
			Jobs.update(varJobId, {$set: {flow: projectId}});
		};

		var flowJobs = Jobs.find({flow: flowId})
		flowJobs.map(revertFlow);
		
		Flows.remove(flowId);
	},

	flowJobDelete: function(flowId, jobTitle) {
		check(Meteor.userId(), String);
		check(flowId, String);
		check(jobTitle, String);

		Flows.update(flowId, {$pull: {jobs: {title: jobTitle}}});
	},

	flowJobAdd: function(flowId, job) {
		check(Meteor.userId(), String);
		check(flowId, String);
		check(job, {
			title: String,
			predecessor: Array,
			tags: Array,
			description: String
		});

		Flows.update(flowId, {$addToSet: {jobs: job}});
	}

}); //end methods