// Initiate collection for tags
// Steps on setting up new collection
// 1: initiate new collection (see below)
// 2: publish in /server/publications.js
// 3: subscribe in /lib/router.js
Tags = new Mongo.Collection('tags');

Meteor.methods({
	tagInsert: function(newTags) {
		check(Meteor.userId(), String);
		check(newTags, Array);

		var tagsCount = newTags.length;

    for(i=0; i<tagsCount; i++) {
      var tag = {
  		  name: newTags[i],
  		  submitted: new Date(),
        updated: new Date()
      };
      // create a new doc only if one of the same name does not exist
      if(Tags.find({name: tag.name}).count() === 0) {
        var tagId = Tags.insert(tag);
      }
    }
	}
});