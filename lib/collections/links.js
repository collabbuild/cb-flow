// Initiate collection for tags
// Steps on setting up new collection
// 1: initiate new collection (see below)
// 2: publish in /server/publications.js
// 3: subscribe in /lib/router.js
Links = new Mongo.Collection('links');

validateLink = function (link) {
  var errors = {};
  if (!link.title)
    errors.title = "Please fill in a name";
  if (!link.url)
    errors.url = "Please fill in a url";
  return errors;
}

Meteor.methods({
  linkInsert: function(addLink, newTags) {
		check(Meteor.userId(), String);
		check(addLink, {
      title: String,
      url: String
    });
    check(newTags, Array);

    var user = Meteor.user();
    
    var link = _.extend(addLink, {
      userId: user._id, 
      author: user.username, 
      submitted: new Date(),
      commentsCount: 0,
      updated: new Date(),
      saved: [],
      savedCount: 0,
      tags: []
    });

    var linkId = Links.insert(link);

    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Links.update(linkId, {$addToSet: {tags: newTags[i]}});
      Tags.update({name: newTags[i]}, {$set: {updated: new Date()}});
    }

    return {
      _id: linkId
    };
	},

  saveLink: function(linkId) {
    check(Meteor.userId(), String);
    check(linkId, String);

    var userId = Meteor.userId();

    Links.update(linkId, {$addToSet: {saved: userId}});
    Links.update(linkId, {$inc: {savedCount: 1}});
  },

  unsaveLink: function(linkId) {
    check(Meteor.userId(), String);
    check(linkId, String);

    var userId = Meteor.userId();

    Links.update(linkId, {$pull: {saved: userId}});
    Links.update(linkId, {$inc: {savedCount: -1}});
  },

  removeLinkAuthor: function(linkId) {
    check(Meteor.userId(), String);
    check(linkId, String);

    var userId = Meteor.userId();

    Links.update(linkId, {$set: {userId: ""}});
    Links.update(linkId, {$set: {author: ""}});
  },


  addTagToLink: function(linkId, newTags) {
    check(Meteor.userId(), String);
    check(linkId, String);
    check(newTags, Array);

    var newTagsLength = newTags.length;

    for(i = 0; i < newTagsLength; i++) {
      var tagName = newTags[i];
      Links.update(linkId, {$addToSet: {tags: tagName}});
    }
  }

});