// Initiate collection for costs
Costs = new Mongo.Collection('costs');

// validateCost = function (cost) {
//   var errors = {};
//   if (cost.title)
//     errors.title = "Please fill in a name";
//   if (cost.amount)
//     errors.amount = "Please fill in an amount";
//   return errors;
// }

Meteor.methods({
  costInsert: function(newCost, newTags) {
		check(Meteor.userId(), String);
		check(newCost, {
      title: String,
      phase: String,
      deadline: String,
      targetCost: String,
      actualCost: String,
      quantity: String,
      unitCost: String,
      description: String,
      projectId: String
    });
    check(newTags, Array);

    var user = Meteor.user();
    
    var cost = _.extend(newCost, {
      userId: user._id, 
      author: user.username,
      tags: [],
      submitted: new Date(),
      commentsCount: 0,
      updated: new Date(),
      saved: [],
      savedCount: 0,
      public: false,
      teamEdit: false,
      paid: false, 
      jobId: false
    });

    var costId = Costs.insert(cost);

    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Costs.update(costId, {$addToSet: {tags: newTags[i]}});
      Tags.update({name: newTags[i]}, {$set: {updated: new Date()}});
    }

    return {
      _id: costId
    };
	},

  costUpdate: function(costId, editCost, newTags) {
		check(Meteor.userId(), String);
		check(costId, String);
		check(editCost, {
      title: String,
      phase: String,
      deadline: String,
      targetCost: String,
      actualCost: String,
      quantity: String,
      unitCost: String,
      description: String
    });
    check(newTags, Array);

		Costs.update(costId, {$set: editCost});
    
    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Costs.update(costId, {$addToSet: {tags: newTags[i]}});
      Tags.update({name: newTags[i]}, {$set: {updated: new Date()}});
    }		
  },

  // saveCost: function(costId) {
  //   check(Meteor.userId(), String);
  //   check(costId, String);

  //   var userId = Meteor.userId();

  //   Costs.update(costId, {$addToSet: {saved: userId}});
  //   Costs.update(costId, {$inc: {savedCount: 1}});
  // },

  // unsaveCost: function(costId) {
  //   check(Meteor.userId(), String);
  //   check(costId, String);

  //   var userId = Meteor.userId();

  //   Costs.update(costId, {$pull: {saved: userId}});
  //   Costs.update(costId, {$inc: {savedCount: -1}});
  // },

  addTagToCost: function(costId, newTags) {
    check(Meteor.userId(), String);
    check(costId, String);
    check(newTags, Array);

    var newTagsLength = newTags.length;

    for(i = 0; i < newTagsLength; i++) {
      var tagName = newTags[i];
      Costs.update(costId, {$addToSet: {tags: tagName}});
    }
  },

  costDelete: function(costId) {
    check(Meteor.userId(), String);
    check(costId, String);
    
    Costs.remove(costId);
  },

  setJobId: function(costId, currentJobId) {
    check(Meteor.userId(), String);
    check(costId, String);
    check(currentJobId, String);

    Costs.update(costId, {$set: {jobId: currentJobId}});
  }


});