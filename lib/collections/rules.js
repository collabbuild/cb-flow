// Initiate collection for tags
// Steps on setting up new collection
// 1: initiate new collection (see below)
// 2: publish in /server/publications.js
// 3: subscribe in /lib/router.js
Rules = new Mongo.Collection('rules');

// validateLink = function (link) {
//   var errors = {};
//   if (!link.title)
//     errors.title = "Please fill in a name";
//   if (!link.url)
//     errors.url = "Please fill in a url";
//   return errors;
// }

Meteor.methods({
  ruleInsert: function(addRule, newTags) {
		check(Meteor.userId(), String);
		check(addRule, {
      title: String,
      ruletext: String,
      equation: String
    });
    check(newTags, Array);

    var user = Meteor.user();
    
    var rule = _.extend(addRule, {
      userId: user._id, 
      author: user.username, 
      submitted: new Date(),
      commentsCount: 0,
      updated: new Date(),
      saved: [],
      savedCount: 0,
      tags: []
    });

    // create new rule doc
    var ruleId = Rules.insert(rule);

    // add tags to the new rule doc
    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Rules.update(ruleId, {$addToSet: {tags: newTags[i]}});
      Tags.update({name: newTags[i]}, {$set: {updated: new Date()}});
    }

    return {
      _id: ruleId
    };
	},

  ruleEdit: function(ruleId, editRule, newTags) {
    check(Meteor.userId(), String);
    check(ruleId, String);
    check(editRule, {
      title: String,
      ruletext: String,
      equation: String
    });
    check(newTags, Array);

    Rules.update(ruleId, {$set: editRule});

    // add tags to the new rule doc
    var tagsCount = newTags.length;
    for(i=0; i<tagsCount; i++) {
      Rules.update(ruleId, {$addToSet: {tags: newTags[i]}});
      Tags.update({name: newTags[i]}, {$set: {updated: new Date()}});
    }
  }

  // saveLink: function(linkId) {
  //   check(Meteor.userId(), String);
  //   check(linkId, String);

  //   var userId = Meteor.userId();

  //   Links.update(linkId, {$addToSet: {saved: userId}});
  //   Links.update(linkId, {$inc: {savedCount: 1}});
  // },

  // unsaveLink: function(linkId) {
  //   check(Meteor.userId(), String);
  //   check(linkId, String);

  //   var userId = Meteor.userId();

  //   Links.update(linkId, {$pull: {saved: userId}});
  //   Links.update(linkId, {$inc: {savedCount: -1}});
  // },

  // removeLinkAuthor: function(linkId) {
  //   check(Meteor.userId(), String);
  //   check(linkId, String);

  //   var userId = Meteor.userId();

  //   Links.update(linkId, {$set: {userId: ""}});
  //   Links.update(linkId, {$set: {author: ""}});
  // },


  // addTagToLink: function(linkId, newTags) {
  //   check(Meteor.userId(), String);
  //   check(linkId, String);
  //   check(newTags, Array);

  //   var newTagsLength = newTags.length;

  //   for(i = 0; i < newTagsLength; i++) {
  //     var tagName = newTags[i];
  //     Links.update(linkId, {$addToSet: {tags: tagName}});
  //   }
  // }

});