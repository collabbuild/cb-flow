Router.configure({
	// default template set to layout.html
  layoutTemplate: 'layout',
  // set the template for loading 
  loadingTemplate: 'loading',
  // set the template for pages that cannot be found
  notFoundTemplate: 'notFound',
  // ensures that the 'Jobs' collection is loaded before the layout is loaded
  waitOn: function() { 
    return [
      Meteor.subscribe('projects'),
      Meteor.subscribe('jobs'),
      Meteor.subscribe('notifications'),
      Meteor.subscribe('tags'),
      Meteor.subscribe('userData'),
      Meteor.subscribe('links'),
      Meteor.subscribe('flows'),
      Meteor.subscribe('rules'),
      Meteor.subscribe('costs')
    ]
  }
});

Router.route('/login', {name: 'loginPage'});

Router.route('/recoverpassword', {name: 'recoverPassword'});

// projects, jobs, and flows
Router.route('/', {name: 'projectsList'});

Router.route('/project', {template: 'projectsList'});

Router.route('/project/:_id', {
  name: 'projectPage',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/new-project', {name: 'newProject'});

Router.route('/project/:_id/info', {
  name: 'projectInfo',
  waitOn: function() {
    // subscribe to publication; see publications.js
    return Meteor.subscribe('activities', this.params._id);
  },  
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/project/:_id/activity', {
  name: 'projectActivity',
  waitOn: function() {
    // subscribe to publication; see publications.js
    return Meteor.subscribe('activities', this.params._id);
  },  
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/project/:_id/submit', {
  name: 'newJob',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/project/:_id/flows', {
  name: 'flowsList',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/project/:_id/schedule', {
  name: 'projectSchedule',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('/flow/:_id', {
  name: 'flowPage',
  data: function() { return Flows.findOne(this.params._id); }
});

Router.route('/job/:_id', {
	name: 'jobPage',
  waitOn: function() {
    return Meteor.subscribe('comments', this.params._id);
  },
	data: function() { return Jobs.findOne(this.params._id); }
});

Router.route('/job/:_id/edit', {
  name: 'jobEdit',
  data: function() { return Jobs.findOne(this.params._id); }
});

// user pages
Router.route('/user/:_id', {
  name: 'userPage',
  data: function() { return Meteor.users.findOne(this.params._id); }
});

Router.route('/usersettings', {name: 'userSettings'});

// tag pages
Router.route('/tags', {name: 'tagsList'});

Router.route('/tags/:_id', {
  name: 'tagLink',
  data: function() { return Tags.findOne(this.params._id); }
});

Router.route('/tags/:_id/rules', {
  name: 'tagRule',
  data: function() { return Tags.findOne(this.params._id); }
});

Router.route('/tags/:_id/jobs', {
  name: 'tagJob',
  data: function() { return Tags.findOne(this.params._id); }
});

Router.route('/tags/:_id/products', {
  name: 'tagProduct',
  data: function() { return Tags.findOne(this.params._id); }
});

// link pages
Router.route('/links', {name: 'linksList'});

Router.route('/link/submit', {name: 'linkSubmit'});

Router.route('/link/:_id', {
  name: 'linkPage',
  waitOn: function() {
    return Meteor.subscribe('commentsForLink', this.params._id);
  },
  data: function() { return Links.findOne(this.params._id); }
});

// rules pages
Router.route('/rule/submit', {name: 'ruleSubmit'});

Router.route('/rule/:_id', {
  name: 'rulePage',
  waitOn: function() {
    return Meteor.subscribe('commentsForRule', this.params._id);
  },
  data: function() { return Rules.findOne(this.params._id); }
});

Router.route('/rule/:_id/edit', {
  name: 'ruleEdit',
  data: function() { return Rules.findOne(this.params._id); }
});

// cost pages
Router.route('project/:_id/cost', {
  name: 'costPage',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('project/:_id/cost/submit', {
  name: 'projectCostSubmit',
  data: function() { return Projects.findOne(this.params._id); }
});

Router.route('job/:_id/cost/submit', {
  name: 'jobCostSubmit',
  data: function() { return Jobs.findOne(this.params._id); }
});

Router.route('/cost/:_id', {
  name: 'costItem',
  waitOn: function() {
    return Meteor.subscribe('commentsForCost', this.params._id);
  },
  data: function() { return Costs.findOne(this.params._id); }
});

Router.route('/cost/:_id/edit', {
  name: 'costEdit',
  data: function() { return Costs.findOne(this.params._id); }
});

// help page
Router.route('/help', {name: 'helpPage'});

// router helpers
var requireLogin = function() {
  // check if user is logged in
  if (! Meteor.user()) {
    if (Meteor.loggingIn()) {
      // see Router.configure block above for loadingTemplate
      this.render(this.loadingTemplate);
    } 
    this.render('loginPage');
  } else {
    this.next();
  }
}

// show the notFound page when data function returns undefined
Router.onBeforeAction('dataNotFound', {only: 'postPage'});
// Router.onBeforeAction('dataNotFound', {only: 'projectList'});

// require login to be able to submit new job
Router.onBeforeAction(requireLogin, {except: ['loginPage', 'recoverPassword', 'projectsList', 'tagsList', 'tagLink', 'tagRule', 'linksList', 'linkPage', 'rulePage']});