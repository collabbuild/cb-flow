// After removing the autopublish package, the collections will need to be published

// Publish 'Jobs' collection
Meteor.publish('jobs', function() {
  return Jobs.find();
});

// 
Meteor.publish('comments', function(jobId) {
	check(jobId, String);
	return Comments.find({jobId: jobId});
});

Meteor.publish('commentsForLink', function(linkId) {
  check(linkId, String);
  return Comments.find({linkId: linkId});
});

Meteor.publish('commentsForRule', function(ruleId) {
  check(ruleId, String);
  return Comments.find({linkId: ruleId});
});

Meteor.publish('commentsForCost', function(costId) {
  check(costId, String);
  return Comments.find({linkId: costId});
});

Meteor.publish('activities', function(projectId) {
	check(projectId, String);
	// change this to only publish team and team members' activity only
	return Activities.find({project: projectId});
});

// returns only the user's notifications
// read is a property of each notification doc
Meteor.publish('notifications', function() {
	return Notifications.find({userId: this.userId, read: false});
});

Meteor.publish('projects', function() {
	return Projects.find();
});

Meteor.publish("userData", function() {
  if (this.userId) {
    return Meteor.users.find(
    	// {_id: this.userId},
    	{},
    	{fields: {'_id': 1, 'username': 1, 'createdAt': 1, 'profile': 1}}
    );
  } else {
    this.ready();
  }	
});

Meteor.publish('tags', function() {
  return Tags.find();
});

Meteor.publish('links', function() {
  return Links.find();
});

Meteor.publish('costs', function() {
  return Costs.find();
});

Meteor.publish('rules', function() {
  return Rules.find();
});

Meteor.publish('flows', function() {
  return Flows.find();
});