Meteor.startup(function () {
  UploadServer.init({
    tmpDir: process.env.PWD + '/public/uploads/tmp',
    uploadDir: process.env.PWD + '/public/uploads/',
	//maxPostSize: 5000,
	maxFileSize: 5000000,
    checkCreateDirectories: true, //create the directories for you
	finished(fileInfo, formFields) {
		console.log("finished");
		console.log(fileInfo.name);
		return fileInfo;	
      //fileInfo.extraData = "12345676";
   }
  });
});