// if (Flows.find().count() === 0) {
//   Flows.insert({
//     title: 'CEE490 FLOW',
//     jobs: {
//       job001: {
//         title: "Job #1",
//         assignee: "Civil",
//         description: "civil job",
//         tags: [#civil],
//         predecessors: [],
//         link: 001
//       },
//       job002: {
//         title: "Job #2",
//         assignee: "Environmental",
//         description: "environmental job",
//         tags: [#wastewater],
//         predecessors: [001],
//         link: 002
//       },
//       job003: {
//         title: "Job #3",
//         assignee: "Geotechnical",
//         description: "geotechnical job",
//         tags: [#soil],
//         predecessors: [001, 002],
//         link: 003
//       }      
//     }
//   });
// }

// if (Meteor.users.find().count === 0) {
//   var userId = Accounts.createUser({
//       username: 'admin',
//       // email: 'none@none.com',
//       password: 'password'
//       // profile: {
//       //     first_name: 'John',
//       //     last_name: 'Doe',
//       //     company: 'ABC',
//       // }
//   });
// }

if (Tags.find().count() === 0) {
  Tags.insert({
    name: "civil",
    submitted: new Date()
  });

  Tags.insert({
    name: "wastewater",
    submitted: new Date()
  });

  Tags.insert({
    name: "stormwater",
    submitted: new Date()
  });

  Tags.insert({
    name: "water",
    submitted: new Date()
  });

  Tags.insert({
    name: "parking",
    submitted: new Date()
  });

  Tags.insert({
    name: "cad",
    submitted: new Date()
  });

  Tags.insert({
    name: "zoning",
    submitted: new Date()
  });

  Tags.insert({
    name: "erosion",
    submitted: new Date()
  });

  Tags.insert({
    name: "structural",
    submitted: new Date()
  });

  Tags.insert({
    name: "geotechnical",
    submitted: new Date()
  });

  Tags.insert({
    name: "traffic",
    submitted: new Date()
  });

  Tags.insert({
    name: "costestimate",
    submitted: new Date()
  });

  Tags.insert({
    name: "proposal",
    submitted: new Date()
  });

  Tags.insert({
    name: "preliminary",
    submitted: new Date()
  });

  Tags.insert({
    name: "submit30",
    submitted: new Date()
  });

  Tags.insert({
    name: "submit60",
    submitted: new Date()
  });

  Tags.insert({
    name: "submit90",
    submitted: new Date()
  });

  Tags.insert({
    name: "submit100",
    submitted: new Date()
  });

  Tags.insert({
    name: "finalsubmit",
    submitted: new Date()
  });

  Tags.insert({
    name: "calc",
    submitted: new Date()
  });

  Tags.insert({
    name: "architectural",
    submitted: new Date()
  });

  Tags.insert({
    name: "mechanical",
    submitted: new Date()
  });

  Tags.insert({
    name: "electrical",
    submitted: new Date()
  });

  Tags.insert({
    name: "hvac",
    submitted: new Date()
  });

  Tags.insert({
    name: "foundation",
    submitted: new Date()
  });

  Tags.insert({
    name: "roof",
    submitted: new Date()
  });

  Tags.insert({
    name: "meeting",
    submitted: new Date()
  });
}