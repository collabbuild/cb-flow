
Meteor.methods({
  'deleteFile': function(url) {
    check(url, String);

    //var upload = Uploads.findOne(_id);
    if (url == null) {
      throw new Meteor.Error(404, 'Upload not found'); // maybe some other code
    }

    UploadServer.delete(url);
//    Uploads.remove(_id);
  }
})