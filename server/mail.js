Meteor.startup (function () {
	process.env.MAIL_URL='smtp://postmaster%40mg.collabbuild.com:75614310115fe07013fa4fa079db3c98@smtp.mailgun.org:587';
	Accounts.emailTemplates.from='no-reply@collabbuild.com';
	Accounts.emailTemplates.sitename='CollabBuild';
});

/*Meteor.methods({
  sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    //actual email sending method
    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  }
});*/

Meteor.methods({
  sendEmail: function (text, email) {
	
    check([text], [String]);
	check([email], [String]);
	
    this.unblock();

    Email.send({
      to: "bugs@collabbuild.com",
	  from: email,
      subject: "Bug Report",
      text: text
    });
  }
});