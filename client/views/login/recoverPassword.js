// set error sessions
Template.recoverPassword.onCreated(function() {
  Session.set('recoverPasswordErrors', {});
});

// error helpers
Template.recoverPassword.helpers({
  errorMessage: function(field) {
    return Session.get('recoverPasswordErrors') [field];
  },
  errorClass: function(field) {
    return !!Session.get('recoverPasswordErrors') [field] ? 'has-error' : '';
  }
});

Template.recoverPassword.events({
	'submit form': function(event){
        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
		var errors={};
		
        Accounts.forgotPassword({email: emailVar}, function (error, result) {
		  if (error) {	
			errors.email=error.reason;
			return Session.set('recoverPasswordErrors', errors);
			
		  } else {
			errors={};
			$('.recover-success').show();
			return Session.set('recoverPasswordErrors', errors);
		  }
		});
    }
});
