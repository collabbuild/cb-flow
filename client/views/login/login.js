// set error sessions
Template.login.onCreated(function() {
  Session.set('loginErrors', {});
});

// error helpers
Template.login.helpers({
  errorMessage: function(field) {
    return Session.get('loginErrors') [field];
  },
  errorClass: function(field) {
    return !!Session.get('loginErrors') [field] ? 'has-error' : '';
  }
});

Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
        var passwordVar = event.target.loginPassword.value;
		
        Meteor.loginWithPassword(emailVar, passwordVar, function (error) {			
			if(error){
				var errors={};
				if (error.reason == "User not found"){
					errors.email=error.reason;
					
					return Session.set('loginErrors', errors);
				}
				if(error.reason == "Incorrect password"){
					errors.password=error.reason;
					
					return Session.set('loginErrors', errors);
				}
				
				//return throwError(error.reason);
			}
			else if (Router.current().route.getName() === 'loginPage') {
				Router.go('projectsList');
			}
		});		
    },
	
	'click .show-register': function (e) {
		$('.login').hide();
		$('.register').show();
  	},
	
});