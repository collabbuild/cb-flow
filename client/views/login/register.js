// set error sessions
Template.register.onCreated(function() {
  Session.set('registerErrors', {});
});

// error helpers
Template.register.helpers({
  errorMessage: function(field) {
    return Session.get('registerErrors') [field];
  },
  errorClass: function(field) {
    return !!Session.get('registerErrors') [field] ? 'has-error' : '';
  }
});

Template.register.events({
    'submit form': function(event) {
        event.preventDefault();
		var firstName = event.target.registerFirst.value;
		var lastName = event.target.registerLast.value;
        var emailVar = event.target.registerEmail.value;
        var passwordVar = event.target.registerPassword.value;
		var verifyPasswordVar = event.target.verifyPassword.value;
		var errors={};
		var userInfo = {
			// properties for user-input
			firstName: firstName,
			lastName: lastName,
			email: emailVar,
		  };
		
		// error validation
		// users must submit the following fields before submit will complete
		var errors = validateUser(userInfo);
		if (errors.firstName || errors.lastName || errors.email )
		  return Session.set('registerErrors', errors);
		
		if (passwordVar == verifyPasswordVar) {
          Accounts.createUser({
            email: emailVar,
            password: passwordVar,
            username: (firstName + lastName).toLowerCase(),
			profile: {
                firstName: firstName,
                lastName: lastName,
                username: (firstName + lastName).toLowerCase()
    		} //close profile
		  }, function (error) {
			if(error){
				
				if (error.reason == "Email already exists."){
					errors.email=error.reason;
					return Session.set('registerErrors', errors);
				} else if (error.reason == "Username already exists.") {
					errors.lastName="A user with this name already exists. Please pick another name.";	
					return Session.set('registerErrors', errors);
				}
				
				//return throwError(error.reason);
			} else if (Router.current().route.getName() === 'loginPage') {
				Router.go('projectsList');
			}
		  }); //closes function error and create user
		} else {
			errors.password = "Passwords don't match";
			return Session.set('registerErrors', errors);
		}
	}, //close submit form
   
	
	'click .show-login': function (e) {
		$('.register').hide();
		$('.login').show();
  	}
});