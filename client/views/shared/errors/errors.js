// Local (client-only) collection
// null sets the collection data to never be saved into the server-side database
Errors = new Mongo.Collection(null);

// function to insert errors into the collection
throwError = function(message) {
	Errors.insert({message: message});
};

// helpers for errors template
Template.errors.helpers({
	// returns a list of errors
	errors: function() {
		return Errors.find();
	}
});

// clear error after 3 seconds
Template.error.onRendered(function() {
	var error = this.data;
	Meteor.setTimeout(function() {
		Errors.remove(error._.id);
	}, 3000);
});