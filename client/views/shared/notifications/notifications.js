Template.notifications.helpers({
  notifications: function() {
    return Notifications.find({userId: Meteor.userId(), read: false});
  },
  notificationCount: function(){
      return Notifications.find({userId: Meteor.userId(), read: false}).count();
  },
  notificationCheck: function(){
	  if (Notifications.find({userId: Meteor.userId(), read: false}).count() > 0) {
		  return true;
	  }
	  else {
		  return false;
	  }
  }
});

Template.clearAllNotifications.events({
  'click .clearall': function() {
    Meteor.call('clearNotifications', function(error, result) {
      if (error)
        return throwError(error.reason);
    });
  }
});



Template.notificationItem.helpers({
  notificationJobPath: function() {
    return Router.routes.jobPage.path({_id: this.jobId});
  }
});

Template.notificationItem.events({
  'click a': function() {
    Notifications.update(this._id, {$set: {read: true}});
  }
});