// Contents:
	// helpers for tagPage
	// events for tagPage
	// helpers for linkRow

// helpers for tagPage
Template.tagProduct.helpers({
	savedLinks: function() {
		var tagName = Template.parentData().name;
		// var pageTag = Template.parentData().name;
		var currentUserId = Meteor.userId();
		return Links.find(			
			{
				$and: [
					// {tags: pageTag},
					{tags: tagName},
					{saved: {$in: [currentUserId]}}
				]
			},
			{sort: {savedCount: -1, updated: -1}}
		);
	},

	links: function() {
		var tagName = Template.parentData().name;
		// var pageTag = Template.parentData().name;
		var currentUserId = Meteor.userId();
		return Links.find(
			{
				$and: [
					// {tags: pageTag},
					{tags: tagName},
					{saved: {$nin: [currentUserId]}}
				]
			},
			{sort: {savedCount: -1, updated: -1}}
		);
	},

  linkhead: function() {
		var tagName = this.name;
		var currentUserId = Meteor.userId();
		var linkheads = [];
		Links.find({tags: {$in: [tagName]}}, {sort: {savedCount: -1, updated: -1}}).map(function(link) {
			var tagsFromLink = link.tags;
			tagsFromLink.map(function(tag) {
				if(tag != tagName)
					linkheads.checkpush(tag);
			});
		});
		return linkheads.sort();
  },

  rules: function() {
		var tagName = Template.currentData();
		var pageTag = Template.parentData().name;
    return Rules.find(
    	{
    		$and: [
					{tags: pageTag},
					{tags: tagName},
    		]
    	}, 
    	{sort: {updated: -1}}
    );
  },

  rulehead: function() {
		var tagName = this.name;
		var currentUserId = Meteor.userId();
		var ruleheads = [];
		Rules.find({tags: {$in: [tagName]}}, {sort: {savedCount: -1, updated: -1}}).map(function(rule) {
			var tagsFromRule = rule.tags;
			tagsFromRule.map(function(tag) {
				if(tag != tagName)
					ruleheads.checkpush(tag);
			});
		});
		return ruleheads.sort();
  },

	projects: function() {
		var currentUserId = Meteor.userId();
		var projects = Projects.find( 
			{$or: [	{members: {$in: [currentUserId] } }, 
							{admin: {$in: [currentUserId]} } ] } 
		);
		return projects;
	},

	jobsInProject: function() {
		var tagName = Template.parentData().name;
		var projectId = this._id;
		return Jobs.find({project: projectId, tags: {$in: [tagName]}});
	},

	jobsWithTag: function() {
		var tagName = Template.parentData().name;
		var projectId = this._id;
		var jobCount = Jobs.find({project: projectId, tags: {$in: [tagName]}}).count();
		if(jobCount > 0) {
			return true;
		} else {
			return false;
		}
	},
	
  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  userName: function() {
  	var authorId = this.userId;
  	return Meteor.users.findOne(authorId).username;
  },

  linkSaved: function() {
  	var currentUserId = Meteor.userId();
  	var userSavedArray = this.saved;
  	return userSavedArray.contains(currentUserId);
  },

  userSaved: function() {
  	var savedCount = this.savedCount;
  	return savedCount > 0;
  },

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  }
});

// click events
Template.tagProduct.events({
 	'click .save-link': function(e) {
    e.preventDefault();

    var linkId = this._id;

    Meteor.call('saveLink', linkId, function(error, result) { 
    	if (error)
	    	return throwError(error.reason);
    });
  },

 	'click .unsave-link': function(e) {
    e.preventDefault();

    var linkId = this._id;

    Meteor.call('unsaveLink', linkId, function(error, result) { 
    	if (error)
	    	return throwError(error.reason);
    });
  },

 //  'click .show-jobs': function (e) {
	//  	$('.filter-buttons .is-active').removeClass('is-active');
	//  	var $this = $(e.target);
	//  	$this.closest('.mdl-button').addClass('is-active');
	// 	$('.tagged-product').hide();
	// 	$('.tagged-rule').hide();
	// 	$('.tagged-link').hide();
	// 	$('.tagged-job').show();
 //  },
  
 // 'click .show-links': function (e) {
	//  	$('.filter-buttons .is-active').removeClass('is-active');
	//  	var $this = $(e.target);
	//  	$this.closest('.mdl-button').addClass('is-active');
	// 	$('.tagged-product').hide();
	// 	$('.tagged-rule').hide();
	// 	$('.tagged-link').show();
	// 	$('.tagged-job').hide();
 //  },
  
 //  'click .show-rules': function (e) {
	//   $('.filter-buttons .is-active').removeClass('is-active');
	//  	var $this = $(e.target);
	//  	$this.closest('.mdl-button').addClass('is-active');
	// 	$('.tagged-product').hide();
	// 	$('.tagged-rule').show();
	// 	$('.tagged-link').hide();
	// 	$('.tagged-job').hide();
 //  },
  
 //  'click .show-products': function (e) {
	//   $('.filter-buttons .is-active').removeClass('is-active');
	//  	var $this = $(e.target);
	//  	$this.closest('.mdl-button').addClass('is-active');
	// 	$('.tagged-product').show();
	// 	$('.tagged-rule').hide();
	// 	$('.tagged-link').hide();
	// 	$('.tagged-job').hide();
 //  },

  'click .show-all-links': function (e) {
  	$('.link-filter-buttons .is-active').removeClass('is-active');
  	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
	 	$('.link-row').show();
  },

  'click .link-filter': function (e) {
  	$('.show-all-links').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
	 	var thisvalue = $this.val();
	 	$('.link-row:not([data-tags~="'+thisvalue+'"])').hide();
  }
});

// Template.tagProduct.rendered = function() {
// 	$('.tagged-product').hide();
// 	$('.tagged-job').hide();
// 	$('.tagged-rule').hide();
// };