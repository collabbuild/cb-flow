// Contents:
	// helpers for tagRule
	// events for tagRule

// helpers for tagRule
Template.tagRule.helpers({
  rules: function() {
		var tagName = Template.parentData().name;
    return Rules.find(
    	{
    		$and: [
					{tags: tagName},
    		]
    	}, 
    	{sort: {updated: -1}}
    );
  },

  taghead: function() {
		var tagName = this.name;
		var currentUserId = Meteor.userId();
		var tagheads = [];
		Rules.find({tags: {$in: [tagName]}}, {sort: {savedCount: -1, updated: -1}}).map(function(rule) {
			var tagsFromRule = rule.tags;
			tagsFromRule.map(function(tag) {
				if(tag != tagName)
					tagheads.checkpush(tag);
			});
		});
		return tagheads.sort();
  },
	
  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  userName: function() {
  	var authorId = this.userId;
  	return Meteor.users.findOne(authorId).username;
  },

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  }
});

// click events
Template.tagRule.events({
	'click .show-all-items': function (e) {
		$('.tag-filter-buttons .is-active').removeClass('is-active');
		var $this = $(e.target);
		$this.closest('.mdl-button').addClass('is-active');
		$('.rule-item').show();
	},

	'click .tag-filter': function (e) {
		$('.show-all-items').removeClass('is-active');
		var $this = $(e.target);
		$this.closest('.mdl-button').addClass('is-active');
		var thisvalue = $this.val();
		$('.rule-item:not([data-tags~="'+thisvalue+'"])').hide();
	}
});