// Contents:
	// helpers for tagJob

// helpers for tagJob
Template.tagJob.helpers({
	projects: function() {
		var currentUserId = Meteor.userId();
		var projects = Projects.find( 
			{$or: [	{members: {$in: [currentUserId] } }, 
							{admin: {$in: [currentUserId]} } ] } 
		);
		return projects;
	},

	jobsInProject: function() {
		var tagName = Template.parentData().name;
		var projectId = this._id;
		return Jobs.find({project: projectId, tags: {$in: [tagName]}});
	},

	jobsWithTag: function() {
		var tagName = Template.parentData().name;
		var projectId = this._id;
		var jobCount = Jobs.find({project: projectId, tags: {$in: [tagName]}}).count();
		if(jobCount > 0) {
			return true;
		} else {
			return false;
		}
	},
	
	tagsList: function() {
		var allTags = this.tags;
		var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
		return actualTags;
	},

	tagId: function() {
		var tagName = this.toString();
		var tagId = Tags.findOne({name: tagName})._id;
		return tagId;
	},

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  }
});