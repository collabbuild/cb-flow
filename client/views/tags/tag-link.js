// Contents:
	// helpers for tagPage
	// events for tagPage
	// helpers for linkRow

// helpers for tagPage
Template.tagLink.helpers({
	savedLinks: function() {
		var tagName = Template.parentData().name;
		var currentUserId = Meteor.userId();
		return Links.find(			
			{
				$and: [
					{tags: tagName},
					{saved: {$in: [currentUserId]}}
				]
			},
			{sort: {savedCount: -1, updated: -1}}
		);
	},

	links: function() {
		var tagName = Template.parentData().name;
		var currentUserId = Meteor.userId();
		return Links.find(
			{
				$and: [
					{tags: tagName},
					{saved: {$nin: [currentUserId]}}
				]
			},
			{sort: {savedCount: -1, updated: -1}}
		);
	},

	linkSaved: function() {
		var currentUserId = Meteor.userId();
		var userSavedArray = this.saved;
		return userSavedArray.contains(currentUserId);
	},

	userSaved: function() {
		var savedCount = this.savedCount;
		return savedCount > 0;
	},

  taghead: function() {
		var tagName = this.name;
		var currentUserId = Meteor.userId();
		var tagheads = [];
		Links.find({tags: {$in: [tagName]}}, {sort: {savedCount: -1, updated: -1}}).map(function(link) {
			var tagsFromLink = link.tags;
			tagsFromLink.map(function(tag) {
				if(tag != tagName)
					tagheads.checkpush(tag);
			});
		});
		return tagheads.sort();
  },
	
  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  userName: function() {
  	var authorId = this.userId;
  	return Meteor.users.findOne(authorId).username;
  },

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  }
});

// click events
Template.tagLink.events({
 	'click .save-link': function(e) {
    e.preventDefault();

    var linkId = this._id;

    Meteor.call('saveLink', linkId, function(error, result) { 
    	if (error)
	    	return throwError(error.reason);
    });
  },

 	'click .unsave-link': function(e) {
    e.preventDefault();

    var linkId = this._id;

    Meteor.call('unsaveLink', linkId, function(error, result) { 
    	if (error)
	    	return throwError(error.reason);
    });
  },

	'click .show-all-items': function (e) {
		$('.tag-filter-buttons .is-active').removeClass('is-active');
		var $this = $(e.target);
		$this.closest('.mdl-button').addClass('is-active');
		$('.link-row').show();
	},

	'click .tag-filter': function (e) {
		var $this = $(e.target);
		$('.show-all-items').removeClass('is-active');
		$this.closest('.mdl-button').addClass('is-active');
		var thisvalue = $this.val();
		$('.link-row:not([data-tags~="'+thisvalue+'"])').hide();
	}
	});

// helpers for linkRow Template
Template.linkRow.helpers({
  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },  

  userName: function() {
  	var authorId = this.userId;
  	return Meteor.users.findOne(authorId).username;
  },

  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  },

  linkSaved: function() {
  	var currentUserId = Meteor.userId();
  	var userSavedArray = this.saved;
  	return userSavedArray.contains(currentUserId);
  },

  linkHasAuthor: function() {
  	var linkOwnerId = this.userId;

  	return (linkOwnerId != "");
  },

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  },
});