// template helpers
Template.projectPage.helpers({
  memberName: function() {
  	var memberId = this.toString();
    var currentMember = Meteor.users.findOne(memberId);
    // var memberName = currentMember.profile.firstName + " " + currentMember.profile.lastName;
    var memberName = currentMember.username;
    return memberName;
  },

  teamMember: function() {
    var currentUserId = Meteor.userId();
    var memberArray = this.members;
    // var memberCount = memberArray.length;
    var adminArray = this.admin;

    return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
    // var adminCount = adminArray.length;
    // for(i = 0; i < adminCount; i++) {
    //   var n = currentUserId.localeCompare(adminArray[i]);
    //   if(n === 0) {
    //     return true;
    //   }
    // }
    // for(i = 0; i < memberCount; i++) {
    //   var n = currentUserId.localeCompare(memberArray[i]);
    //   if(n === 0) {
    //     return true;
    //   }
    // }
    // return false;
  },

  jobs: function() {
  	var projectId = this._id;
    return Jobs.find({project: projectId}, {sort: {isChecked: -1, isLocked: +1, deadline: +1}});
  },

  isAdmin: function() {
    var currentUserId = Meteor.userId();
    var adminArray = this.admin;
    return adminArray.contains(currentUserId);
  }


});

Template.projectPage.events({
  // expands or collapses job-item
  'click .mdl-card__title': function (e) {
    var $this = $(e.target);
		$this.next('.job-item-body').toggle();
  },
  
  'click .show-all': function (e) {
	 	$('.filter-buttons .is-active').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
		$('.job-item').show();
    $('.deleted').closest('.job-item').hide();
  },
  
 'click .show-open': function (e) {
	 	$('.filter-buttons .is-active').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
		$('.locked').closest('.job-item').hide();
		$('.done').closest('.job-item').hide();
		$('.deleted').closest('.job-item').hide();
		$('.open').closest('.job-item').show();
  },
  
  'click .show-locked': function (e) {
	  $('.filter-buttons .is-active').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
		$('.open').closest('.job-item').hide();
		$('.done').closest('.job-item').hide();
		$('.deleted').closest('.job-item').hide();
		$('.locked').closest('.job-item').show();
  },
  
  'click .show-done': function (e) {
	  $('.filter-buttons .is-active').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
		$('.locked').closest('.job-item').hide();
		$('.open').closest('.job-item').hide();
		$('.deleted').closest('.job-item').hide();
		$('.done').closest('.job-item').show();
  },
  
  'click .show-deleted': function (e) {
	  $('.filter-buttons .is-active').removeClass('is-active');
	 	var $this = $(e.target);
	 	$this.closest('.mdl-button').addClass('is-active');
		$('.locked').closest('.job-item').hide();
		$('.done').closest('.job-item').hide();
		$('.open').closest('.job-item').hide();
		$('.deleted').closest('.job-item').show();
  }
});

Template.projectPage.rendered = function() {
	$('.deleted').closest('.job-item').hide();
	$('.done').closest('.job-item').hide();
	$('.locked').closest('.job-item').hide();
};