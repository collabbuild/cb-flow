// template helpers
Template.projectActivity.helpers({


  activities: function() {
    var currentProjectId = this._id;
    return Activities.find({project: currentProjectId}, {sort: {submitted: -1}, limit: 25});
  },

  convertDate: function(timestamp) {
    var curr_date = timestamp.getDate();
    if (curr_date < 10) {
      curr_date="0"+curr_date;
    }
    var curr_month = timestamp.getMonth();
    curr_month++;
    if (curr_month < 10) {
      curr_month="0"+curr_month;
    }
    var curr_year = timestamp.getFullYear();
    var curr_hours = timestamp.getHours();
    var curr_minutes = timestamp.getMinutes();
    if (curr_minutes < 10) {
      curr_minutes="0"+curr_minutes;
    }
    result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
    return result;
  }
});