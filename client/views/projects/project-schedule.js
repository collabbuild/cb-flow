// template helpers
Template.projectSchedule.helpers({
	memberName: function() {
		var memberId = this.toString();
		var currentMember = Meteor.users.findOne(memberId);
		var memberName = currentMember.username;
		return memberName;
	},

	teamMember: function() {
		var currentUserId = Meteor.userId();
		var memberArray = this.members;
		var adminArray = this.admin;
		return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
	},

	isAdmin: function() {
		var currentUserId = Meteor.userId();
		var adminArray = this.admin;
		return adminArray.contains(currentUserId);
	},

	// this helper is only for use in the team member table
	// because it requires use of parentData()
	isAdminInChild: function() {
		var currentUserId = Meteor.userId();
		var adminArray = Template.parentData().admin;
		var adminCount = adminArray.length;
		return adminArray.contains(currentUserId);
	},

	activities: function() {
		var currentProjectId = this._id;
		return Activities.find({project: currentProjectId}, {sort: {submitted: -1}, limit: 25});
	},

	settings: function() {
		return {
			position: "bottom",
			limit: 10,
			rules: [
				{
				// token: '@',
				collection: Meteor.users,
				field: "username",
				matchAll: true,
				template: Template.userLabel
				}
			]
		};
	},

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
	}
});

Template.projectSchedule.events({
	'click .submit': function(e) {
		e.preventDefault();
		var projectId = this._id;
		var assignment = {
			title: $('#assignment').val(),
			leader: []
		};

		$('#assignment').val('');

		if(assignment.title.length > 0) {
			Meteor.call('addAssignment', projectId, assignment, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .assign': function(e) {
		e.preventDefault();
		var projectId = Template.currentData()._id;
		var userId = Meteor.userId();
		var assignment = {
			title: this.title,
			leader: userId
		};

		Meteor.call('addLeader', projectId, assignment, function(error, result) {
			if (error)
				return throwError(error.reason);
		});
	},

	'click .unassign': function(e) {
		e.preventDefault();
		var projectId = Template.currentData()._id;
		var userId = Meteor.userId();
		var assignment = {
			title: this.title,
			leader: userId
		};

		Meteor.call('unassignLeader', projectId, assignment, function(error, result) {
			if (error)
				return throwError(error.reason);
		});
	},

	'click .remove': function(e) {
		e.preventDefault();

		if(confirm("Delete assignment?")) {
			var projectId = Template.currentData()._id;
			var assignment = {
				title: this.title
			};

			Meteor.call('deleteAssignment', projectId, assignment, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .invite': function(e) {
		e.preventDefault();
		var projectId = Template.currentData()._id;
		var invitedUserName = $('#invite').val();
		$('#invite').val('');

		if(invitedUserName.length > 0) {
			var invitedUserId = Meteor.users.findOne({username: invitedUserName})._id;

			Meteor.call('inviteUser', projectId, invitedUserId, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .uninvite': function(e) {
		e.preventDefault();

		if(confirm("Cancel invite?")) {
			var projectId = Template.currentData()._id;
			var invitedUserId = this.toString();

			Meteor.call('uninviteUser', projectId, invitedUserId, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .makeAdmin': function(e) {
		e.preventDefault();

		if(confirm("Grant user admin priveleges?")) {
			var projectId = Template.currentData()._id;
			var currentUserId = this.toString();

			Meteor.call('makeAdmin', projectId, currentUserId, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	}
});