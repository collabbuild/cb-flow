// set error sessions
Template.newProject.onCreated(function() {
	Session.set('projectSubmitErrors', {});
});

// error helpers
// 'field' is title, discipline, deadline, etc.
Template.newProject.helpers({
	errorMessage: function(field) {
		return Session.get('projectSubmitErrors') [field];
	},
	errorClass: function(field) {
		return !!Session.get('projectSubmitErrors') [field] ? 'has-error' : '';
	},

	defaultphases: [
		"Conceptualization",
		"Criteria Design",
		"Detailed Design",
		"Implementation Documents",
		"Agency Review",
		"Buyout",
		"Construction",
		"Closeout"
	]
});

// events for newJob template
Template.newProject.events({
	'click .submit': function(e) {
		e.preventDefault();

		var phaseObject = $('.phasecheckbox:checked').map(function() {
			return $(this).val();
		});

		var phaseArray = phaseObject.get();

		var projectAttributes = {
			title: $('#title').val(),
			deadline: $('#deadline').val(),
			phases: phaseArray,
			currency: $('.currencycheckbox:checked').val(),
			invitations: $('.invitecheckbox:checked').val()
		};

		console.log(projectAttributes.currency, projectAttributes.invitations);

		var errors = validateProject(projectAttributes);
		if (errors.title)
			return Session.set('projectSubmitErrors', errors);

		Meteor.call('projectInsert', projectAttributes, function(error, result) {
			if (error)
				return throwError(error.reason);

			$('#title').val('');
			$('#deadline').val('');
			Router.go('projectPage', {_id: result._id});
		});
	}
});

//Datepicker for Deadline Date
// Template.newProject.rendered = function() {
//     $( '#datepicker' ).datepicker({
// 		dateFormat: "yy/mm/dd"
// 	});
// };