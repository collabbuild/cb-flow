// template helpers
Template.projectsList.helpers({
	projects: function() {
		var currentUserId = Meteor.userId();
		return Projects.find({
			$or: [{members: {$in: [currentUserId]}}, {admin: {$in: [currentUserId]}}],
			closed: {$ne: true}
		});
	},

	invitedProjects: function() {
		var currentUserId = Meteor.userId();
		return Projects.find({invited: {$in: [currentUserId]}});
	},

	closedProjects: function() {
		var currentUserId = Meteor.userId();
		return Projects.find({
			$or: [{members: {$in: [currentUserId]}}, {admin: {$in: [currentUserId]}}],
			closed: true
		});
	},

	gotInvitations: function() {
		var currentUserId = Meteor.userId();
		var invitationCount = Projects.find({invited: {$in: [currentUserId]}}).count();
		if(invitationCount > 0)
			return true;
	},

	tags: function() {
		return Tags.find({}, {sort: {updated: -1, name: 1}, limit: 15});
	},

	links: function() {
		return Links.find({}, {sort: {updated: -1}, limit: 8});
	},

	tagsList: function() {
		var allTags = this.tags;
		var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
		return actualTags;
	},

	tagId: function() {
		var tagName = this.toString();
		var tagId = Tags.findOne({name: tagName})._id;
		return tagId;
	},

	domain: function() {
		var a = document.createElement('a');
		a.href = this.url;
		return a.hostname;
	}	
});

Template.projectsListTable.helpers({
	memberCount: function() {
		var memberArray = this.members;
		var memberCount = memberArray.length;
		var adminArray = this.admin;
		var adminCount = adminArray.length;
		return memberCount + adminCount;
	},

	teamMember: function() {
		var currentUserId = Meteor.userId();
		var memberArray = this.members;
		var adminArray = this.admin;

		return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
	},

	invitedMember: function() {
		var currentUserId = Meteor.userId();
		var invitedArray = this.invited;

		return (invitedArray.contains(currentUserId));
	},

	isCreator: function() {
		return Meteor.userId() === this.userId;
	},

	jobQuantity: function() {
		var projectId = this._id;
		return Jobs.find({project: projectId, deleted: false}).count();
	},

	jobDoneQuantity: function() {
		var projectId = this._id;
		return Jobs.find({project: projectId, isChecked: true}).count();
	}
});

Template.projectsList.events({
	'change select': function(e) {
		var projectType = $("#projectType").val();

		if(projectType === "active") {
			$('.active-table').show();
			$('.invited-table').show();
			$('.closed-table').hide();
		}

		if(projectType === "closed") {
			$('.active-table').hide();
			$('.invited-table').hide();
			$('.closed-table').show();
		}
	}	
});

Template.projectsListTable.events({
	'click .join': function(e) {
		e.preventDefault();

		var projectId = this._id;
		var userId = Meteor.userId();

		Meteor.call('addUser', projectId, userId, function(error, result) {
			if (error)
				return throwError(error.reason);

			Router.go('projectPage', {_id: projectId});
		});
	},

	'click .leave': function(e) {
		e.preventDefault();

		if(confirm("Leave this project?")) {
			var projectId = this._id;
			var userId = Meteor.userId();

			Meteor.call('removeUser', projectId, userId, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .decline': function(e) {
		e.preventDefault();

		if(confirm("Decline this invitation?")) {
			var projectId = this._id;
			var userId = Meteor.userId();

			Meteor.call('declineUser', projectId, userId, function(error, result) {
				if (error)
					return throwError(error.reason);
			});
		}
	},

	'click .close': function(e) {
		e.preventDefault();

		if(confirm("Close this project?")) {
			var projectId = this._id;
			var userId = Meteor.userId();
			var creatorId = this.userId;

			if(userId = creatorId) {
				Meteor.call('closeProject', projectId, function(error, result) {
					if (error)
						return throwError(error.reason);
				});
			}
		}
	},

	'click .reopen': function(e) {
		e.preventDefault();

		if(confirm("Open this project?")) {
			var projectId = this._id;
			var userId = Meteor.userId();
			var creatorId = this.userId;

			if(userId = creatorId) {
				Meteor.call('reopenProject', projectId, function(error, result) {
					if (error)
						return throwError(error.reason);
				});
			}
		}
	}
});

Template.projectsList.rendered = function() {
	$('.closed-table').hide();
};