Template.linkSubmit.onCreated(function() {
  Session.set('linkSubmitErrors', {});
});

Template.linkSubmit.helpers({
  errorMessage: function(field) {
    return Session.get('linkSubmitErrors') [field];
  },
  errorClass: function(field) {
    return !!Session.get('linkSubmitErrors') [field] ? 'has-error' : '';
  },

  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }
});

Template.linkSubmit.events({
  'submit form': function(e) {
    e.preventDefault();

    var link = {
    	title: $(e.target).find('[name=title]').val(),
    	url: $(e.target).find('[name=url]').val()
    }

    var newTags = $(e.target).find('[name=tags]').val().split(" ");
    newTags = newTags.filter(function(m){ return m.toString().length > 2 });
    newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
    newTags = newTags.map(function(o) { return o.substring(1) });

    var errors = validateLink(link);
    if (errors.title)
      return Session.set('linkSubmitErrors', errors);
    if (errors.url)
      return Session.set('linkSubmitErrors', errors);

    Meteor.call('tagInsert', newTags, function(error, result) {
      // display the error to the user and abort
      if (error)
        return throwError(error.reason);      
    });

    Meteor.call('linkInsert', link, newTags, function(error, result) {
    	if (error) {
      	return throwError(error.reason);
      } else {
        $(e.target).find('[name=title]').val("");
        $(e.target).find('[name=url]').val("");
        $(e.target).find('[name=tags]').val("");        
      }

      Router.go('linkPage', {_id: result._id});
    });
 	}
});