Template.linksList.helpers({
	links: function() {
		return Links.find({}, {sort: {updated: -1}});
	},
	
	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  },

  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  }
});