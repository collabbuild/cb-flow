// set error sessions
// Template.ruleSubmit.onCreated(function() {
//   Session.set('linkSubmitErrors', {});
// });

Template.costEdit.helpers({
//   errorMessage: function(field) {
//     return Session.get('linkSubmitErrors') [field];
//   },
//   errorClass: function(field) {
//     return !!Session.get('linkSubmitErrors') [field] ? 'has-error' : '';
//   },
  
	projectTitle: function() {
		var currentProjectId = this.projectId;
		return Projects.findOne(currentProjectId).title;
	},

  settings: function() {
  	return {
			position: "bottom",
			limit: 10,
			rules: [
				{
					token: '#',
					collection: Tags,
					field: "name",
					// options: '',
					matchAll: true,
					// filter: { type: "autocomplete" },
					template: Template.tagLabel
				}
			]
 		};
	}
});

Template.costEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var costId = this._id;

		var cost = {
			title: $(e.target).find('[name=title]').val(),
			phase: $(e.target).find('[name=phase]').val(),
			deadline: $(e.target).find('[name=deadline]').val(),
			targetCost: $(e.target).find('[name=targetcost]').val(),
			actualCost: $(e.target).find('[name=actualcost]').val(),
			quantity: $(e.target).find('[name=quantity]').val(),
			unitCost: $(e.target).find('[name=unitcost]').val(),
			description: $(e.target).find('[name=description]').val()
		};

		var newTags = $(e.target).find('[name=tags]').val().split(" ");
		newTags = newTags.filter(function(m){ return m.toString().length > 2 });
		newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
		newTags = newTags.map(function(o) { return o.substring(1) });

		// var errors = validateLink(link);
		// if (errors.title)
		//   return Session.set('linkSubmitErrors', errors);
		// if (errors.url)
		//   return Session.set('linkSubmitErrors', errors);

		if(cost.title != "") {
			Meteor.call('tagInsert', newTags, function(error, result) {
				// display the error to the user and abort
				if (error)
					return throwError(error.reason);
			});

			Meteor.call('costUpdate', costId, cost, newTags, function(error, result) {
				if (error) {
					return throwError(error.reason);
				} else {
					$(e.target).find('[name=title]').val("");
					$(e.target).find('[name=tags]').val("");
					$(e.target).find('[name=deadline]').val("");
					$(e.target).find('[name=targetcost]').val("");
					$(e.target).find('[name=actualcost]').val("");
					$(e.target).find('[name=quantity]').val("");
					$(e.target).find('[name=unitcost]').val("");
					$(e.target).find('[name=description]').val("");
				}

				Router.go('costItem', {_id: costId});
			}); 
		}
	},

	'click .delete': function(e) {
		e.preventDefault();

		if (confirm("Delete this cost item?")) {
			var currentCostId = this._id;
			var currentProjectId = this.projectId;

			Meteor.call('costDelete', currentCostId, function(error, result) {
				if (error)
					throwError(error.reason);
				else
					Router.go('costPage', {_id: currentProjectId});
			});
		}
	}
});

//Datepicker for Deadline Date
Template.costEdit.rendered = function() {
	$( '#datepicker' ).datepicker({
		dateFormat: "yy/mm/dd"
	});
};