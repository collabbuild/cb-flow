Template.costPage.helpers({
	costs: function() {
		var projectId = Template.parentData()._id;
		var currentPhase = Template.currentData();
		return Costs.find({projectId: projectId, phase: currentPhase}, {sort: {actualCost: +1}});
	},

	phases: function() {
		return [
			"Conceptualization",
			"Criteria Design",
			"Detailed Design",
			"Implementation Documents",
			"Agency Review",
			"Buyout",
			"Construction",
			"Closeout",
			"None"
		];
	}
});

Template.costRow.helpers({
	tagsList: function() {
		var allTags = this.tags;
		var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
		return actualTags;
	},

	tagId: function() {
		var tagName = this.toString();
		var tagId = Tags.findOne({name: tagName})._id;
		return tagId;
	},

	userName: function() {
		var authorId = this.userId;
		return Meteor.users.findOne(authorId).username;
	},

	showTargetCost: function() {
		if(this.targetCost != "")
			return ("$" + this.targetCost);
	},

	showActualCost: function() {
		if(this.actualCost != "")
			return ("$" + this.actualCost);
	},

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
	}
});