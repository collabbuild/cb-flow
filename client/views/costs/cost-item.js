Template.costItem.helpers({
	ruleOwner: function() {
		return Meteor.userId() === this.userId;
	},

	comments: function() {
		return Comments.find({linkId: this._id}, {sort: {submitted: +1}});
	},

	tagsList: function() {
		var allTags = this.tags;
		var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
		return actualTags;
	},

	tagId: function() {
		var tagName = this.toString();
		var tagId = Tags.findOne({name: tagName})._id;
		return tagId;
	},

	gotDeadline: function() {
		return (this.deadline != "")
	},

	gotQuantity: function() {
		return (this.quantity != "")
	},

	gotUnitCost: function() {
		return (this.unitCost != "")
	},  

	showTargetCost: function() {
		if(this.targetCost != "")
			return ("$" + this.targetCost);
		else return ("---");
	},

	showActualCost: function() {
		if(this.actualCost != "")
			return ("$" + this.actualCost);
		else return ("---");
	},

	projectTitle: function() {
		var currentProjectId = this.projectId;
		return Projects.findOne(currentProjectId).title;
	},

	showJobTitle: function() {
		if(this.jobId)
			return Jobs.findOne(this.jobId).title;
	},

	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
	}
});