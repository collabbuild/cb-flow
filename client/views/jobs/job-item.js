// helpers
Template.jobItem.helpers({
  teamMember: function() {
    var currentUserId = Meteor.userId();
    var projectId = this.project;
    var currentProject = Projects.findOne(projectId);
    var adminArray = currentProject.admin;
    var memberArray = currentProject.members;

    return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
  },
  
  convertDate: function(timestamp) {
    var curr_date = timestamp.getDate();
	if (curr_date < 10) {
		curr_date="0"+curr_date;
	}
    var curr_month = timestamp.getMonth();
    curr_month++;
	if (curr_month < 10) {
		curr_month="0"+curr_month;
	}
    var curr_year = timestamp.getFullYear();
	var curr_hours = timestamp.getHours();
	var curr_minutes = timestamp.getMinutes();
	if (curr_minutes < 10) {
		curr_minutes="0"+curr_minutes;
	}
    result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
    return result;
  },
  
  descendantTitle: function() {
    // convert array object to string
    var descendantId = this.toString();
    // find the job document matching the descendant id and get the title field
    var descendantTitle = Jobs.findOne(descendantId).title;
    return descendantTitle;
  },

  predecessorTitle: function() {
    var predecessorId = this.toString();
    if(predecessorId === "" || predecessorId === "None") {
      return;
    } else {
    var predecessorTitle = Jobs.findOne({_id: predecessorId}).title;
    return predecessorTitle;
    }
  },

  hasFlow: function() {
    return !(this.flow === this.project);
  },

  hasPredOrDesc: function() {
    return (this.predecessor.length > 0 || this.descendant.length > 0);
  },

  flowName: function() {
    var flowId = this.flow;
    return Flows.findOne(flowId).name;
  },

  predecessorCount: function() {
    return this.predecessor.length;
  },

  descendantCount: function() {
    return this.descendant.length;
  },

  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  cardBackground: function() {
    if(this.isLocked || this.deleted) {
      return "locked-background";
    } else if(this.isChecked) {
      return "done-background";
    } else {
      return "mdl-color--white";
    }
  },
  
  ifDescription : function(description) {
    if(description != "") {
    	return description;
    } else {
    	return "None";
    }
  },
  
  ifDropboxLink : function(description) {
    if(dropboxlink != "") {
		console.log(dropboxlink);
    	return dropboxlink;
    } else {
    	return "None";
    }
  },
  
  findUsername: function(userId) {
	  var username = Meteor.users.findOne({_id: userId}).profile.username;
	  return username;
  }
});

// events
Template.jobItem.events({
  "click .toggle-checked": function(e) {
    e.preventDefault();

    var currentJobId = this._id;
    var descendantArray = this.descendant;

    if (this.isChecked) {
      if (confirm("Mark job as OPEN?")) {
        // Set the checked property to the opposite of its current value
        // this.checked returns true or false
        Meteor.call('setChecked', currentJobId, false, descendantArray, function(error, result) {
          if (error) 
            throwError(error.reason);
        });
      }
    }
    else {
      if (confirm("Mark job as DONE?")) {
        // Set the checked property to the opposite of its current value
        // this.checked returns true or false
        Meteor.call('setChecked', currentJobId, true, descendantArray, function(error, result) {
          if (error) 
            throwError(error.reason);
          $('.open').closest('.job-item').show();
        });
      }
    }
  },

  'click .delete': function(e) {
    e.preventDefault();

    if (confirm("Delete this job?")) {
      var currentJobId = this._id;
      var predecessorArray = this.predecessor;
      var descendantArray = this.descendant;
      var currentProjectId = this.project;

      // Jobs.remove(currentJobId);
      Meteor.call('jobDelete', currentJobId, predecessorArray, descendantArray, function(error, result) {
        if (error) 
          throwError(error.reason);
        $('.deleted').closest('.job-item').hide();
      });
    }
  },

  // to restore jobs from deleted
  "click .deleted": function(e) {
    e.preventDefault();
    if(confirm("Restore this job?")) {
      var currentJobId = this._id;
      
      Meteor.call('jobUndelete', currentJobId, function(error, result) {
        if (error) 
          throwError(error.reason);
              
        $('.locked').closest('.job-item').hide();
        $('.done').closest('.job-item').hide();
        $('.open').closest('.job-item').hide();
      });

    }
  }
});

