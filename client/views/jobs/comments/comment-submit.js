Template.commentSubmit.onCreated(function() {
	Session.set('commentSubmitErrors', {});
});

Template.commentSubmit.helpers({
	errorMessage: function(field) {
		return Session.get('commentSubmitErrors')[field];
	},
	errorClass: function (field) {
		return !!Session.get('commentSubmitErrors')[field] ? 'has-error' : '';
	}
});

Template.commentSubmit.events({
	'submit form': function(e, template) {
		e.preventDefault();

		// this var used for comments submitted from job-page and link-page
		var currentPageId = template.data._id;

		// for comments submitted from job-page
		if(Jobs.findOne(currentPageId)){
			var $body = $(e.target).find('[name=body]');
			var comment = {
				body: $body.val(),
				jobId: currentPageId
			};

			var errors = {};
			if (! comment.body) {
				errors.body = "Please write some content";
				return Session.set('commentSubmitErrors', errors);
			}

			$body.val('');

			Meteor.call('commentInsert', comment, function(error, commentId) {
				if (error)
					throwError(error.reason);
			});

			var activity = {
				jobId: currentPageId,
				activityBody: "Commented on a job" /*+" "+template.data.project*/
			};

			Meteor.call('activityInsert', activity, function(error, result) {
				if (error)
					throwError(error.reason);
			});
		}

		// for comments submitted from link-page, rule-page, or cost-page
		else {
			var $body = $(e.target).find('[name=body]');
			var comment = {
				body: $body.val(),
				linkId: currentPageId
			};

			var errors = {};
			if (! comment.body) {
				errors.body = "Please write some content";
				return Session.set('commentSubmitErrors', errors);
			}

			$body.val('');

			Meteor.call('commentInsertForLink', comment, function(error, commentId) {
				if (error)
					throwError(error.reason);
			});
		}
	}
});