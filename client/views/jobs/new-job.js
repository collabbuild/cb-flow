// set error sessions
Template.newJob.onCreated(function() {
  Session.set('jobSubmitErrors', {});
});

// error helpers
// 'field' is title, discipline, deadline, etc.
Template.newJob.helpers({
  errorMessage: function(field) {
    return Session.get('jobSubmitErrors') [field];
  },
  errorClass: function(field) {
    return !!Session.get('jobSubmitErrors') [field] ? 'has-error' : '';
  },
  
  jobs: function(currentProjectId) {
    return Jobs.find({project: currentProjectId, deleted: false});
  },

  newJobNo: function() {
    var currentProjectId = Template.currentData()._id;
    var totalJobs = Jobs.find({project: currentProjectId}).count();
    var newJobNo = (totalJobs + 1);
    return newJobNo;
  },

  // for autocomplete in tag field
  // see shared/autocomplete.html for templates
  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        // {
        //   token: '@',
        //   collection: Projects,
        //   field: "members",
        //   matchAll: true,
        //   template: Template.userLabel
        // },
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }
});

// events for newJob template
Template.newJob.events({
  'submit form': function(e) {
    e.preventDefault();

    var projectId = this._id;

    // select2 is the submitted predecessor list
    // ensures all items in select2 are selected
    $('#select2 option').each(function(i) {
      $(this).attr("selected", "selected");
    });

    // if this is greater than 0, then the user has set some 
    // predecessors while editing the job
    if($('#select2 option').length > 0) {
      var predAttr = {
        predecessor: Array.from($(e.target).find('[name=predecessor2]').val())
      };  
    } else {
      var predAttr = {
        predecessor: []
      };    
    }

    var job = _.extend(predAttr, {
      // properties for user-input
      title: $(e.target).find('[name=title]').val(),
      assignee: $(e.target).find('[name=assignee]').val(),
      deadline: $(e.target).find('[name=deadline]').val(),
      description: $(e.target).find('[name=description]').val(),
	  	dropboxlink: $(e.target).find('[name=dropboxlink]').val()
    });  

    // take the string and split it into an array where spaces occur
    var newTags = $(e.target).find('[name=tags]').val().split(" ");
    // remove strings that are not greater than two character
    newTags = newTags.filter(function(m){ return m.toString().length > 2 });
    newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
    newTags = newTags.map(function(o) { return o.substring(1) });

    // calls function; /collections/job.js
    var errors = validateJob(job);
    if (errors.title || errors.discipline || errors.deadline || errors.predecessor || errors.dropboxlink)
      return Session.set('jobSubmitErrors', errors);

    // create a new tag for the tags the user creates
    // call the tagInsert method; see tags.js collection
    Meteor.call('tagInsert', newTags, function(error, result) {
      // display the error to the user and abort
      if (error)
        return throwError(error.reason);
    });

    // start flow code

    var predecessorArray = job.predecessor;
    var predecessorLength = predecessorArray.length;

    // if user sets no predecessors
    if(predecessorLength === 0) {
      flowId = projectId;
      // set flowId to project id
      // when this is true, this means that the current job is not part
      // of a flow
    }

    // if user sets one or more predecessor
    if(predecessorLength > 0) {
      // check for unique flow ids
      // function to get flow id for map function below
      function getFlow(varId) {
        return Jobs.findOne(varId).flow;
      }

      // see function above for getFlow
      var flowArray = predecessorArray.map(getFlow);
      var flowUniques = _.uniq(flowArray);
      var flowUniquesLength = flowUniques.length;

      // if there is only one unique flow id and
      // if the flow id of the predecessor is the project id
      // means that the predecessor job does not belong to a flow
      if(flowUniquesLength === 1 && flowUniques[0] === projectId) {
        
        var flowName = "FLOW w/ " + job.title;

        // call flowInsert which will set the flow fleld of the predecessor
        // job to a new flow id
        // predecessorArray is an array with one entry with predecessor's id
        Meteor.call('flowInsert', predecessorArray, flowName, projectId, function(error, result) {
          if (error)
            return throwError(error.reason);
          flowId = result._id;
        });
      }

      // if there is only one unique flow id and
      // if the flow id of the predecessor is not project id
      // means that the predecessor job is already part of a flow
      else if(flowUniquesLength === 1 && flowUniques[0] != projectId) {
        // add this new job to the predecessor's flow
        // set the flowId to the same flow as the predecessor job
        flowId = flowUniques[0];
        // update the existing flow so that it contains this new job 
        // see the call for flowUpdate in the call for jobInsert
      }      

      // if there are more than one unique flow ids
      // that means that they could be
      // - flow ids (one or more)
      // - current project id
      // either way a new flow id should be generated
      else {
        var flowName = "FLOW w/ " + job.title;

        Meteor.call('flowInsert', predecessorArray, flowName, projectId,  function(error, result) {
          if (error)
            return throwError(error.reason);
          flowId = result._id;
        });        
      }
    }

    // end flow code

    // method: go to jobs.js in collection folder
    Meteor.call('jobInsert', projectId, job, newTags, function(error, result) {
      // display the error to the user and abort
      if (error)
        return throwError(error.reason);

      // with jobInsert, initiate activityInsert to log the creation of new job
  		var activity = {
  		  jobId: result._id,
  		  activityBody: "Created a job "
  		};
  		
  		Meteor.call('activityInsert', activity, function(error, result) {
  		  if (error){
          throwError(error.reason);
        }
  		});

      var jobId = result._id;

      // this if-statement is executed when a new job is created
      // first situation: the user does not set any predecessors
      // the flow id will be set to project id and the following 
      // if-statement will not run
      // second situation: the user sets a predecessor
      // the if-statement below will run in this case
      // the if-statement will add the new job to the flow document
      if(flowId != projectId) {
        // close off other flows that are involved
        if(flowUniquesLength > 1) {
          flowUniques.map(function(varFlowId) {
            // var varFlowId = Jobs.findOne(varPredId).flow;
            if(varFlowId != projectId) {
              Meteor.call('flowUpdate', varFlowId, jobId, function(error, result) {
                if (error)
                  return throwError(error.reason);
              });
            }
          });
        }

        // update the existing flow so that it contains this new job
        Meteor.call('flowUpdate', flowId, jobId, function(error, result) {
          if (error)
            return throwError(error.reason);
        });        
      }

      Router.go('jobPage', {_id: result._id});
    });
  },

  'click #select1 .switch': function(e) {
    e.preventDefault();
    return !$(e.target).remove().appendTo('#select2');
  },

  'click #select2 .switch': function(e) {
    e.preventDefault();
    return !$(e.target).remove().appendTo('#select1');
  }
});

//Datepicker for Deadline Date
Template.newJob.rendered = function() {
    $( '#datepicker' ).datepicker({
		dateFormat: "yy/mm/dd"
	});
};

// code to change the color of the tag-pill; not currently active
// Template.userPill.labelClass = function() {
//   if (this._id === Meteor.userId()) {
//     return "self";
//   } else if (this.profile.online === true) {
//     return "user-online";
//   } else {
//     return "user-offline";
//   }
// };