Template.jobPage.helpers({
	comments: function() {
		// return the comments for the jobItem that matches the id
		return Comments.find({jobId: this._id}, {sort: {submitted: +1}});
	},

	projectName: function() {
		return Projects.findOne({_id: this.project}).title;
	},

  teamMember: function() {
    var currentUserId = Meteor.userId();
    var currentProject = Projects.findOne({_id: this.project});
    var memberArray = currentProject.members;
    var adminArray = currentProject.admin;

    return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
  },

	jobCosts: function() {
		return Costs.find({jobId: this._id}, {sort: {updated: -1}});
	},

	gotJobCosts: function() {
		return (Costs.find({jobId: this._id}).count() > 0);
	},	

	showActualCost: function() {
    if(this.actualCost != "")
      return ("$" + this.actualCost);
    else return ("---");
  },

  totalCost: function() {
  	var totalCost = 0;

  	Costs.find({jobId: this._id}).map(function(cost) {
  		totalCost += parseFloat(cost.actualCost);
  	});

  	return ("$" + totalCost);
  }
});