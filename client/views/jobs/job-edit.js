Template.jobEdit.onCreated(function() {
  Session.set('jobEditErrors', {});
});


Template.jobEdit.helpers({
  errorMessage: function(field) {
    return Session.get('jobEditErrors') [field];
  },

  errorClass: function(field) {
    return !!Session.get('jobEditErrors') [field] ? 'has-error' : '';
  },

  jobs: function(currentProjectId) {
    var currentJobId = this._id;
    var predecessorArray = this.predecessor;
    var descendantArray = this.descendant;
    var combineArray = predecessorArray.concat(descendantArray);
    var restrictId = combineArray.concat([currentJobId]);
	
    // restrictId is returning the expected array but the code
    // below does not exclude the descendant jobs.
    // This means the user can still set a descendant as a predecessor.
    // predecessor selector added to mitigate this issue until we 
    // come up with a better solution.
    return Jobs.find({
      project: currentProjectId,
      _id: {$nin: restrictId}, 
      predecessor: {$ne: currentJobId},
      deleted: false 
    });
  },

  projectName: function() {
    return Projects.findOne({_id: this.project}).title;
  },

  hasFlow: function() {
    return !(this.flow === this.project);
  },

  predecessorTitle: function() {
    var predecessorId = this.toString();
    if(predecessorId === "" || predecessorId === "None") {
      return;
    } else {
    var predecessorTitle = Jobs.findOne({_id: predecessorId}).title;
    return predecessorTitle;
    }
  },

  thereArePredecessors: function() {
    var predecessorArrayLength = this.predecessor.length;
    if(predecessorArrayLength === 0) {
      return false;
    } else {
      return true;
    }
  },

  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  assignments: function() {
    var assignments = Projects.findOne(this.project).assignments;
    return assignments;
  },

  // for autocomplete in tag field
  // see shared/autocomplete.html for templates
  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        // {
        //   token: '@',
        //   collection: Meteor.users,
        //   field: "username",
        //   matchAll: true,
        //   template: Template.userLabel
        // },
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }
});

// events: submit and delete
Template.jobEdit.events({
  'submit form': function(e) {
    e.preventDefault();
		
  	var oldJobProperties = Template.currentData();
  	
  	var currentJobId = this._id;
    var currentJobTitle = this.title;
    var projectId = this.project;

  	var commentUpdates = "";
	
    // ensures all items in select2 are selected
    $('#select2 option').each(function(i) {
      $(this).attr("selected", "selected");
    });


    // if this is greater than 0, then the user has set some 
    // predecessors while editing the job
    if($('#select2 option').length > 0) {
      var predAttr = {
        predecessor: Array.from($(e.target).find('[name=predecessor2]').val())
      };
    } else {
      var predAttr = {
        predecessor: []
      };    
    }

    var jobProperties = _.extend(predAttr, {
      // properties for user-input
      title: $(e.target).find('[name=title]').val(),
      assignee: $(e.target).find('[name=assignee]').val(),
      deadline: $(e.target).find('[name=deadline]').val(),
      description: $(e.target).find('[name=description]').val(),
			dropboxlink: $(e.target).find('[name=dropboxlink]').val()
    });
	
    var addTags = $(e.target).find('[name=tags]').val().split(" ");
    addTags = addTags.filter(function(m){ return m.toString().length > 2 });
    addTags = addTags.filter(function(n){ return n.substring(0,1) === "#" });
    addTags = addTags.map(function(o) { return o.substring(1) });
	
    // error validation
    // users must submit the following fields before submit will complete
    var errors = validateJob(jobProperties);
    if (errors.title || errors.discipline || errors.deadline || errors.predecessor || errors.dropboxlink)
      return Session.set('jobEditErrors', errors);

    // create a new tag for the tags the user creates
    // call the tagInsert method; see tags.js collection
    Meteor.call('tagInsert', addTags, function(error, result) {
      // display the error to the user and abort
      if (error)
        return throwError(error.reason);      
    }); 


    // start flow code

    // initiate global flow id var
    flowId = this.flow;

    // if the current job being edited is not already part of
    // a flow, then follow the same operation of testing to
    // see if the predecessors are already part of a flow
    if(flowId === projectId) {
      var predecessorArray = jobProperties.predecessor;
      var predecessorLength = predecessorArray.length;

      // if user sets no predecessors
      if(predecessorLength === 0) {
        newFlowId = projectId;
        // set flowId to project id
        // when this is true, this means that the current job is not part
        // of a flow
      }

      // if user sets one or more predecessor
      if(predecessorLength > 0) {
        // check for unique flow ids
        // function to get flow id for map function below
        function getFlow(varId) {
          return Jobs.findOne(varId).flow;
        }

        // see function above for getFlow
        var flowArray = predecessorArray.map(getFlow);
        var flowUniques = _.uniq(flowArray);
        var flowUniquesLength = flowUniques.length;

        // if there is only one unique flow id and
        // if the flow id of the predecessor is the project id
        // means that the predecessor job does not belong to a flow
        if(flowUniquesLength === 1 && flowUniques[0] === projectId) {
          
          var flowName = "FLOW w/ " + jobProperties.title;

          // call flowInsert which will set the flow fleld of the predecessor
          // job to a new flow id
          // predecessorArray is an array with one entry with predecessor's id
          Meteor.call('flowInsert', predecessorArray, flowName, projectId, function(error, result) {
            if (error)
              return throwError(error.reason);
            newFlowId = result._id;
          });
        }

        // if there is only one unique flow id and
        // if the flow id of the predecessor is not project id
        // means that the predecessor job is already part of a flow
        else if(flowUniquesLength === 1 && flowUniques[0] != projectId) {
          // add this new job to the predecessor's flow
          // set the flowId to the same flow as the predecessor job
          newFlowId = flowUniques[0];
          // update the existing flow so that it contains this new job 
          // see the call for flowUpdate in the call for jobInsert
        }      


        // if there are more than one unique flow ids
        // that means that they could be
        // - flow ids (one or more)
        // - current project id
        // either way a new flow id should be generated
        else {
          var flowName = "FLOW w/ " + jobProperties.title;

          Meteor.call('flowInsert', predecessorArray, flowName, projectId,  function(error, result) {
            if (error)
              return throwError(error.reason);
            newFlowId = result._id;
          });        
        }
      }
    }
    // end flow code


    // set the new properties; see Method:jobUpdate in jobs.js collection file
    Meteor.call('jobUpdate', jobProperties, currentJobId, addTags, function(error, result) {
  	  //Check if fields were updated
  	  if (oldJobProperties.title != jobProperties.title) {
  	   		commentUpdates += "Title updated from "+oldJobProperties.title+" to "+jobProperties.title+";\n";
  	  };
  	  if (oldJobProperties.assignee != jobProperties.assignee) {
  			commentUpdates += "Discipline updated from "+oldJobProperties.assignee+" to "+jobProperties.assignee+";\n" 
  	  };
  	  if (oldJobProperties.deadline != jobProperties.deadline) {
  			commentUpdates +="Deadline updated from "+oldJobProperties.deadline+" to "+jobProperties.deadline+";\n"
  	  };
  	  if (oldJobProperties.description != jobProperties.description) {
		  if (oldJobProperties.description == "")
			  {oldJobProperties.description="None"}
		  if (jobProperties.description == "")
			  {jobProperties.description="None"}
  		  	commentUpdates += "Description updated from "+oldJobProperties.description+" to "+jobProperties.description+";\n"
  	  };

		  var oldPredecessor = oldJobProperties.predecessor;
          var newPredecessor = jobProperties.predecessor;
		  
		  var is_same = (oldJobProperties.predecessor.length == jobProperties.predecessor.length) && oldJobProperties.predecessor.every(function(element, index) {
				return element === jobProperties.predecessor[index]; 
			});

		  if (is_same === false) {	
			  if(newPredecessor.length === 0) {
				  newPredName = "None";
			  } else {
				  var newPredName = newPredecessor.map(function(obj) {
  				  var getTitle = Jobs.findOne({_id: obj}).title;
  				  return getTitle;
				  })
			  }
			  if(oldPredecessor.length === 0) {
				oldPredName = "None";
			  } else {
  				var oldPredName = oldPredecessor.map(function(obj) {
  				  var getTitle = Jobs.findOne({_id: obj}).title;
  				  return getTitle;
  				})
			  }
				  
				commentUpdates += "Predecessor updated from "+oldPredName+" to "+newPredName+";\n";
			};
         
      if (error) {
        // display the error to the user
        throwError(error.reason);
      } else {
        //if theres no updates, do nothing
        if (commentUpdates === "") {
	  		  //do nothing
  	    } else {
	  		  //else post comment
  				var comment = {
  					jobId: currentJobId,
            body: commentUpdates
  				} 
				
			  	// Activity Insert
					var activity = {
					  jobId: currentJobId,
					  activityBody: "Edited a job"
					}
			
					Meteor.call('activityInsert', activity, function(error, result) {
					  if (error){
						  throwError(error.reason);
					  }
					});
					
				  Meteor.call('commentInsert', comment, function(error, commentId) {
            if (error){
					    throwError(error.reason);
            }
			    });
        };

        if(flowId === projectId) {
          // if newFlowId = projectId, then the current job had no 
          // predecessors and still does not have predecessors.
          // no flow function will run

          // if newFlowId does not = projectId, then the current job
          // had no predecessors at first, but now has some.
          // the following code will run if this is the case.
          if(newFlowId != projectId) {
            // close off other flows that are involved
            if(flowUniquesLength > 1) {
              flowUniques.map(function(varFlowId) {
                // var varFlowId = Jobs.findOne(varPredId).flow;
                if(varFlowId != projectId) {
                  Meteor.call('flowUpdate', varFlowId, currentJobId, function(error, result) {
                    if (error)
                      return throwError(error.reason);
                  });
                }
              });
            }

            // update the existing flow so that it contains this new job
            Meteor.call('flowUpdate', newFlowId, currentJobId, function(error, result) {
              if (error)
                return throwError(error.reason);
            });          
          }
        } // end else block

        Router.go('jobPage', {_id: currentJobId});
      }
    }); // end Meteor.call(jobUpdate)
  }, // end submit form

  // delete function
  'click .delete': function(e) {
    e.preventDefault();

    if (confirm("Delete this job?")) {
      var currentJobId = this._id;
      var predecessorArray = this.predecessor;
      var descendantArray = this.descendant;
      var currentProjectId = this.project;

      // Jobs.remove(currentJobId);
      Meteor.call('jobDelete', currentJobId, predecessorArray, descendantArray, function(error, result) {
        if (error)
          throwError(error.reason);
        else
          Router.go('projectPage', {_id: currentProjectId});
      });
    }
  },

  'click #select1 .switch': function(e) {
    e.preventDefault();
    return !$(e.target).remove().appendTo('#select2');
  },

  'click #select2 .switch': function(e) {
    e.preventDefault();
    return !$(e.target).remove().appendTo('#select1');
  },

  'click .remove-tag': function(e) {
    e.preventDefault();
    var removeButton = $(e.target);
    var tagPill = removeButton.parent();
    var tagName = this.toString();
    var currentJobId = Template.currentData()._id;
    //console.log(currentJobId, tagName);
    Meteor.call('tagDelete', currentJobId, tagName, function(error, result) {
      if (error)
        return throwError(error.reason);
    });
  }
});

//Datepicker for Deadline Date
Template.jobEdit.rendered=function() {
    $( "#datepicker" ).datepicker({
		dateFormat: "yy/mm/dd"	
	});
}