job: workflow task
	attributes:
		- time allotted or estimated
		- discipline assigned / main discipline / assignee
		- description
		- due date
		- submittals: attachments required to complete the job
		- tags (for search, filter, sort)
			- discipline
			- phase
		- dependencies
		- comments


job-item: 
	the interface of a job

	job-item.html - template
	job-item.js - template helper


jobs-list:
	list the job-items in order of priority
	user home page

	priority: 