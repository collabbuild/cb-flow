Template.userInfo.helpers({
	
  	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
	},

	madeJobs: function() {
		var userId = this._id;
		return (Jobs.find({userId: userId}).count() > 0);
	},

	jobCount: function() {
		var userId = this._id;
		return Jobs.find({userId: userId}).count();
	},

	madeComments: function() {
		var userId = this._id;
		return (Comments.find({userId: userId}).count() > 0);		
	},

	commentCount: function() {
		var userId = this._id;
		return Comments.find({userId: userId}).count();
	},

	madeLinks: function() {
		var userId = this._id;
		return (Links.find({userId: userId}).count() > 0);
	},

	linkCount: function() {
		var userId = this._id;
		return Links.find({userId: userId}).count();
	}
});