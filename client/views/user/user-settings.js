Template.userSettings.onCreated(function() {
  Session.set('userSettingsErrors', {});
  
  //for uploader
  Uploader.init(this);
});

Template.userSettings.rendered = function () {
    Uploader.render.call(this);
	Uploader.finished = function(index, fileInfo) {
	  	var firstName = Meteor.user().profile.firstName;
		var lastName = Meteor.user().profile.lastName;
		var username = Meteor.user().profile.username;
		var oldURL = Meteor.user().profile.profileURLpath;
		Meteor.call('deleteFile', oldURL);
		var profileURL = '/uploads/' + fileInfo.name;
		Meteor.users.update(Meteor.userId(), {$set: {profile: {
				firstName: firstName,
				lastName: lastName,
				username: username,
				profileURL: profileURL,
				profileURLpath: fileInfo.path
			}}
		});
		$('.upload-progress').css("background-color", "#4CAF50");    
 	};
};

Template.userSettings.helpers({
  errorMessage: function(field) {
    return Session.get('userSettingsErrors') [field];
  },

  errorClass: function(field) {
    return !!Session.get('userSettingsErrors') [field] ? 'has-error' : '';
  },
  
  loadLastEmail: function () {
	  var email = Meteor.user().emails;
	  var lastEmail = email[email.length - 1].address;
	  return lastEmail;
  },
  
 
  
  'infoLabel': function() {
    var instance = Template.instance();

    // we may have not yet selected a file
    var info = instance.info.get()
    if (!info) {
      return;
    }

    var progress = instance.globalInfo.get();

    // we display different result when running or not
    return progress.running ?
      info.name + ' - ' + progress.progress + '% - [' + progress.bitrate + ']' :
      info.name + ' - ' + info.size + 'B';
  },
  'progress': function() {
    return Template.instance().globalInfo.get().progress + '%';
  }
});

// events: submit and delete
Template.userSettings.events({
  'submit .usersettings-form': function(e) {
    e.preventDefault();
	var errors={};	
  	var oldFirstName = Meteor.user().profile.firstName;
	var oldLastName = Meteor.user().profile.lastName;
	var oldEmail = Meteor.user().emails[0].address;
	var profileURL = Meteor.user().profile.profileURL;
	var profileURLpath = Meteor.user().profile.path;
	
	var newInfo = {
        // properties for user-input
        firstName: $(e.target).find('[name=firstName]').val(),
        lastName: $(e.target).find('[name=lastName]').val(),
        email: $(e.target).find('[name=email]').val(),
      };
  		
    // error validation
    // users must submit the following fields before submit will complete
    var errors = validateUser(newInfo);
    if (errors.firstName || errors.lastName || errors.email )
      return Session.set('userSettingsErrors', errors);
	  $('.username-success').hide();
	  $('.email-success').hide();

	//if the first or last name changed
	if (oldFirstName != newInfo.firstName || oldLastName != newInfo.lastName) {
		var newUsername = (newInfo.firstName + newInfo.lastName).toLowerCase();
		
		
		Meteor.call('updateUsername', newUsername, function(error){
		  	if (error) {
				if (error.reason == "Username already exists.") {
					errors.lastName="A user with this name already exists. Please pick another name.";
					$('.username-success').hide();
					return Session.set('userSettingsErrors', errors);
				} 
			} else {
				//if username is available, then change first and last profile names
				Meteor.users.update(Meteor.userId(), {$set: {profile: {
					firstName: newInfo.firstName,
					lastName: newInfo.lastName,
					username: newUsername,
					profileURL: profileURL,
					profileURLpath: profileURLpath
					}}
				});
				
				$('.username-success').show();
				return Session.set('userSettingsErrors', errors);
			}
		});
		//return Session.set('userSettingsErrors', errors);
	}
	
	//if email changed
	if (oldEmail != newInfo.email) {
		var newEmail=newInfo.email;
		
		Meteor.call('addNewEmail', newEmail, function(error){
		  	if (error) {
				errors.email=error.reason;
				return Session.set('userSettingsErrors', errors);
				$('.email-success').hide();
				
			}
			else {
				var emailsCount = Meteor.user().emails.length;
				
				if (emailsCount > 1 ) {
					Meteor.call('removeOldEmail', oldEmail, function(error){
						if (error) {
							errors.email=error.reason;
							return Session.set('userSettingsErrors', errors);
							$('.email-success').hide();
						} else {	
							$('.email-success').show();
							return Session.set('userSettingsErrors', errors);
						}
					});
				} else {
					errors.email = "Error adding email address. Please refresh page.";
					return Session.set('userSettingsErrors', errors);
				}
			}
			
			
		});
		
	}
  },
  

  'submit .reset-password-form': function(e) {
    e.preventDefault();
	var errors={};	
	var oldPassword = $(e.target).find('[name=oldPassword]').val();
	var newPassword = $(e.target).find('[name=newPassword]').val();
	var verifyPassword = $(e.target).find('[name=verifyPassword]').val();
	
	if (newPassword == verifyPassword) {
		Accounts.changePassword(oldPassword, newPassword, function (error) {	
			if(error){
				errors.oldPassword=error.reason;	
				return Session.set('userSettingsErrors', errors);	
			} else {
				$('.password-success').show();
				return Session.set('userSettingsErrors', errors);	
			}
		})
			
	} else {
		errors.newPassword = "Passwords don't match";
		return Session.set('userSettingsErrors', errors);
	}
  },
  
  'click .start': function (e) {
        Uploader.startUpload.call(Template.instance(), e);
  },
  
});

