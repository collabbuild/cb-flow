Template.rulePage.helpers({
	ruleOwner: function() {
		return Meteor.userId() === this.userId;
	},

	comments: function() {
		// return the comments for the jobItem that matches the id
		return Comments.find({linkId: this._id}, {sort: {submitted: +1}});
	},

  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  convertDate: function(timestamp) {
    var curr_date = timestamp.getDate();
    if (curr_date < 10) {
      curr_date="0"+curr_date;
    }
    var curr_month = timestamp.getMonth();
    curr_month++;
    if (curr_month < 10) {
      curr_month="0"+curr_month;
    }
    var curr_year = timestamp.getFullYear();
    var curr_hours = timestamp.getHours();
    var curr_minutes = timestamp.getMinutes();
    if (curr_minutes < 10) {
      curr_minutes="0"+curr_minutes;
    }
    result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
    return result;
  },
  
  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }
});

// Template.rulePage.events({
//   'click .add-tags': function(e) {
//     e.preventDefault();

//     var linkId = this._id;

//     var newTags = $('#addTags').val().split(" ");
//     newTags = newTags.filter(function(m){ return m.toString().length > 2 });
//     newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
//     newTags = newTags.map(function(o) { return o.substring(1) });

//     $('#addTags').val("");

//     Meteor.call('tagInsert', newTags, function(error, result) {
//       // display the error to the user and abort
//       if (error)
//         return throwError(error.reason);      
//     });

//     Meteor.call('addTagToLink', linkId, newTags, function(error, result) {
//       // display the error to the user and abort
//       if (error)
//         return throwError(error.reason);      
//     });
//   },

//  	'click .remove-author': function(e) {
//     e.preventDefault();

//    	if(confirm("Remove yourself as the author of this link?")) {
// 	    var linkAuthor = this.userId;
// 	    var currentUserId = Meteor.userId();

// 	    if(linkAuthor === currentUserId){ 
// 		    var linkId = this._id;

// 		    Meteor.call('removeLinkAuthor', linkId, function(error, result) { 
// 		    	if (error)
// 			    	return throwError(error.reason);
// 		    });
// 		  }
// 		}
//   }
// });