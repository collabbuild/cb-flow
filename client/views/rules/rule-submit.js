// set error sessions
// Template.ruleSubmit.onCreated(function() {
//   Session.set('linkSubmitErrors', {});
// });

Template.ruleSubmit.helpers({
//   errorMessage: function(field) {
//     return Session.get('linkSubmitErrors') [field];
//   },
//   errorClass: function(field) {
//     return !!Session.get('linkSubmitErrors') [field] ? 'has-error' : '';
//   },
  equationPreview: function() {
    var preview = $('#equation').val();
    return preview;
  },
	
	convertDate: function(timestamp) {
		var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}
		var curr_month = timestamp.getMonth();
		curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}
		var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}
		result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
		return result;
  },

  tagsList: function() {
    var allTags = this.tags;
    var actualTags = allTags.filter(function(n) { return n.toString().length > 1});
    return actualTags;
  },

  tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }
});


Template.ruleSubmit.events({
  'submit form': function(e) {
    e.preventDefault();

    var rule = {
    	title: $(e.target).find('[name=title]').val(),
      ruletext: $(e.target).find('[name=ruletext]').val(),
      equation: $(e.target).find('[name=equation]').val()
    };

    var newTags = $(e.target).find('[name=tags]').val().split(" ");
    newTags = newTags.filter(function(m){ return m.toString().length > 2 });
    newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
    newTags = newTags.map(function(o) { return o.substring(1) });

    // var errors = validateLink(link);
    // if (errors.title)
    //   return Session.set('linkSubmitErrors', errors);
    // if (errors.url)
    //   return Session.set('linkSubmitErrors', errors);

    if(newTags.length > 0 && rule.title != "" && (rule.ruletext != "" || rule.equation != "")) {

      Meteor.call('tagInsert', newTags, function(error, result) {
        // display the error to the user and abort
        if (error)
          return throwError(error.reason);      
      });

      Meteor.call('ruleInsert', rule, newTags, function(error, result) {
      	if (error) {
        	return throwError(error.reason);
        } else {
          $(e.target).find('[name=title]').val("");
          $(e.target).find('[name=ruletext]').val("");
          $(e.target).find('[name=equation]').val("");
          $(e.target).find('[name=tags]').val("");
        }
        
        Router.go('rulePage', {_id: result._id});
      });
    }
 	},

  'input #equation': function(e) {
    Meteor.setTimeout(function() {
      var preview = $('#equation').val();
      
      $('#equationPreview').text("$$" + preview + "$$");
      MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    }, 100);
  }
});