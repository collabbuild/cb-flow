// template helpers
Template.flowsList.helpers({
  flowsUser: function() {
    var projectId = this._id;
    var currentUserId = Meteor.userId();
    return Flows.find({userId: currentUserId, project: {$ne: projectId}});
  },

  flowsProject: function() {
    var projectId = this._id;
    return Flows.find({project: projectId});
  },

  flowsPublic: function() {
    return Flows.find({public: true});
  }

  // flowFromProject: function() {
  //   return (this.project === Template.parentData()._id);
  // },
});

Template.flowRow.helpers({
  jobsCount: function() {
    var jobsCount = this.jobs.length;
    return jobsCount;
  },

  projectName: function() {
    var projectId = this.project;
    return Projects.findOne(projectId).title;
  },

  flowOwner: function() {
    return (this.userId === Meteor.userId());
  },

  teamMember: function() {
    var currentUserId = Meteor.userId();
    var currentProject = Projects.findOne(Template.parentData()._id);
    var adminArray = currentProject.admin;
    var memberArray = currentProject.members;

    return (adminArray.contains(currentUserId) || memberArray.contains(currentUserId));
  }
});

Template.flowRow.events({
  'click .publish': function(e) {
    e.preventDefault();

    if(confirm("Do you want to make this FLOW public and share it with all users?")) {
      var flowId = this._id;

      Meteor.call('flowPublish', flowId, function(error, result) {
        if (error)
          return throwError(error.reason);
      });
    }
  },

  'click .load-flow': function(e) {
    e.preventDefault();

    if(confirm("Do you want to load this flow into your project?")) {
      var flowId = this._id;
      var projectId = Template.parentData()._id;

      Meteor.call('flowLoad', flowId, projectId, function(error, result) {
        if (error)
          return throwError(error.reason);

        Router.go('projectPage', {_id: projectId});
      });
    }
  }
});