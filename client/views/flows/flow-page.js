Template.flowPage.helpers({
	flowOwner: function() {
		return (this.userId === Meteor.userId());
	},

  flowOwnerInChild: function() {
    return (Template.parentData().userId === Meteor.userId());
  },

	tagId: function() {
    var tagName = this.toString();
    var tagId = Tags.findOne({name: tagName})._id;
    return tagId;
  },

  memberName: function() {
  	var memberId = this.toString();
    var currentMember = Meteor.users.findOne(memberId);
    var memberName = currentMember.username;
    return memberName;
  },  

	comments: function() {
		// return the comments for the jobItem that matches the id
		return Comments.find({jobId: this._id});
	},

  hasPublishers: function() {
    return (this.publishers.length > 0);
  },

	convertDate: function(timestamp) {
    var curr_date = timestamp.getDate();
		if (curr_date < 10) {
			curr_date="0"+curr_date;
		}

    var curr_month = timestamp.getMonth();
    curr_month++;
		if (curr_month < 10) {
			curr_month="0"+curr_month;
		}

    var curr_year = timestamp.getFullYear();
		var curr_hours = timestamp.getHours();
		var curr_minutes = timestamp.getMinutes();
		if (curr_minutes < 10) {
			curr_minutes="0"+curr_minutes;
		}

    result = curr_hours + ":" + curr_minutes + " on " + curr_year+ "/" + curr_month + "/" +curr_date;
    return result;
  },

  jobArray: function() {
  	jobArray = this.jobs;
  	return jobArray;
  },
  
  settings: function() {
    return {
      position: "bottom",
      limit: 10,
      rules: [
        // {
        //   token: '@',
        //   collection: Projects,
        //   field: "members",
        //   matchAll: true,
        //   template: Template.userLabel
        // },
        {
          token: '#',
          collection: Tags,
          field: "name",
          // options: '',
          matchAll: true,
          // filter: { type: "autocomplete" },
          template: Template.tagLabel
        }
      ]
    };
  }  
});


Template.flowPage.events({

  'click .rename-flow': function(e) {
    e.preventDefault();

    var flowId = this._id;
    var newName = $('#rename').val();

    $('#rename').val('');

		if(newName.length > 0) {    
	    Meteor.call('flowRename', flowId, newName, function(error, result) {
	      if (error)
	        return throwError(error.reason);
	    });
  	}
  },

  'click .delete': function(e) {
    e.preventDefault();  

    if(confirm("Remove this FLOW from your account?")) {
      var flowId = this._id;
      var projectId = this.project;

      Meteor.call('flowDelete', flowId, function(error, result) {
        if (error)
          return throwError(error.reason);

        Router.go('flowsList', {_id: projectId});
      });
    }
  },

  'click .deletejob': function(e) {
    e.preventDefault();

    if(confirm("Remove this job from the flow?")) {
      var flowId = Template.currentData()._id;
      var jobTitle = this.title;

      Meteor.call('flowJobDelete', flowId, jobTitle, function(error, result) {
        if (error)
          return throwError(error.reason);
      });
    }

  },

  'click .submit': function(e) {
    e.preventDefault();

    var flowId = this._id;

    // take the string and split it into an array where spaces occur
    var newTags = $("#tags").val().split(" ");
    // remove strings that are not greater than two character
    newTags = newTags.filter(function(m){ return m.toString().length > 2 });
    newTags = newTags.filter(function(n){ return n.substring(0,1) === "#" });
    newTags = newTags.map(function(o) { return o.substring(1) });


    var job = {
      title: $('#title').val(),
      predecessor: Array.from($('#predecessor').val()),
      tags: newTags,
      description: $('#description').val(),
    };

    $('#title').val('');
    $('#predecessor').val('');
    $('#tags').val('');
    $('#description').val('');

    Meteor.call('tagInsert', newTags, function(error, result) {
      // display the error to the user and abort
      if (error)
        return throwError(error.reason);
    });

    if(job.title.length > 0) {
      Meteor.call('flowJobAdd', flowId, job, function(error, result) {
        if (error)
          return throwError(error.reason);
      });
    }

  }
});