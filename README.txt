File Structure
	/client: code in the /client directory only runs on the client
	/lib: files in /lib are loaded before anything else
	/private: (not used)
	/public: static assets (fonts, images, etc.) go here
	/server: code in the /server only runs on the server


	Any main.* file is loaded after everything else
	Everything else runs on both client and server and is loaded alphabetically based on file name.